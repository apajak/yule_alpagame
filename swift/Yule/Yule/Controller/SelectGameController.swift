//
//  SelectGameController.swift
//  Yule
//
//  Created by PAJAK Alexandre on 11/08/2022.
//

import UIKit

class SelectgameController: UIViewController {
    
    //MARK: - Gobal var
    var selectedGame = ""
    //MARK: - outlet
    @IBOutlet weak var button_Barbu: UIButton!
    
    @IBOutlet weak var scrollView: UIView!{
        didSet{
            // Passwd Field design
            let cornerRAdius = 10
            scrollView.layer.cornerRadius = CGFloat(cornerRAdius)
            scrollView.layer.borderColor = UIColor.white.cgColor
            scrollView.layer.borderWidth = 2
            scrollView.layer.masksToBounds = (cornerRAdius != 0)
        }
    }
    
    
    @IBOutlet weak var BarbuView: UIView!{
//        didSet{
//            // Passwd Field design
//            let cornerRAdius = 10
//            BarbuView.layer.cornerRadius = CGFloat(cornerRAdius)
//            BarbuView.layer.borderColor = UIColor.init(red: 1, green: 0.49, blue: 0, alpha: 1).cgColor
//            BarbuView.layer.borderWidth = 2
//            BarbuView.layer.masksToBounds = (cornerRAdius != 0)
//        }
        didSet{
            // Passwd Field design
            let cornerRAdius = 10
            BarbuView.layer.cornerRadius = CGFloat(cornerRAdius)
            BarbuView.layer.borderColor = UIColor.white.cgColor
            BarbuView.layer.borderWidth = 2
            BarbuView.layer.masksToBounds = (cornerRAdius != 0)
        }
    }
    
    
    
    
    
    
    //MARK: - Action
    @IBAction func button_Barbu_GetPressed(_ sender: Any) {
        selectedGame = "BARBU"
    }
//    @IBAction func changeViewGetPressed(_ sender: UIButton) {
//        countstruct.count += 1
//        print(countstruct.count)
//        let targetVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectgameController") as! SelectgameController
//        self.navigationController?.pushViewController(targetVC, animated: false)
//    }
    //MARK: - Functions
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
    if segue.destination is LobbyController {
    let vc = segue.destination as? LobbyController
        vc?.selectedGame = selectedGame
        vc?.isMaster = true
    }
    }
    //MARK: - ViewDidoad
    override func viewDidLoad() {
        super.viewDidLoad()
        let navigationBarAppearace = UINavigationBar.appearance()
        navigationBarAppearace.barTintColor = UIColor.init(red: 1, green: 0.49, blue: 0, alpha: 1)
        navigationBarAppearace.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
    }
}


    //MARK: - Extention (Delegate)

