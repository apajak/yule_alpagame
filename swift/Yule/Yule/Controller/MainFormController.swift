//
//  MainFormController.swift
//  Yule
//
//  Created by PAJAK Alexandre on 15/06/2022.
//

import Foundation
import UIKit


class MainFormController: UIViewController {
    
    //MARK: - Gobal var
    var activeTextField : UITextField? = nil
    
    //MARK: - outlet
    @IBOutlet weak var UserNameField: UITextField!{
        didSet {
            let orangePlaceholderText = NSAttributedString(string: "Pseudo",
                                                        attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(red: 1, green: 0.49, blue: 0, alpha: 1)])
            
            UserNameField.attributedPlaceholder = orangePlaceholderText
        }
    }
    @IBOutlet weak var LastNameFied: UITextField!{
        didSet {
            let orangePlaceholderText = NSAttributedString(string: "Nom",
                                                        attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(red: 1, green: 0.49, blue: 0, alpha: 1)])
            
            LastNameFied.attributedPlaceholder = orangePlaceholderText
        }
    }
    @IBOutlet weak var FirstNameField: UITextField!{
        didSet {
            let orangePlaceholderText = NSAttributedString(string: "Prénom",
                                                        attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(red: 1, green: 0.49, blue: 0, alpha: 1)])
            
            FirstNameField.attributedPlaceholder = orangePlaceholderText
        }
    }
    @IBOutlet weak var MailField: UITextField!{
        didSet {
            let orangePlaceholderText = NSAttributedString(string: "Adresse mail",
                                                        attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(red: 1, green: 0.49, blue: 0, alpha: 1)])
            
            MailField.attributedPlaceholder = orangePlaceholderText
        }
    }
    @IBOutlet weak var PsswdField: UITextField!{
        didSet {
            let orangePlaceholderText = NSAttributedString(string: "Mot de passe",
                                                        attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(red: 1, green: 0.49, blue: 0, alpha: 1)])
            
            PsswdField.attributedPlaceholder = orangePlaceholderText
        }
    }
    @IBOutlet weak var VerifyPasswdField: UITextField!{
        didSet {
            let orangePlaceholderText = NSAttributedString(string: "confirmation mot de passe",
                                                        attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(red: 1, green: 0.49, blue: 0, alpha: 1)])
            
            VerifyPasswdField.attributedPlaceholder = orangePlaceholderText
        }
    }
    @IBOutlet weak var vaidateButton: UIButton!{
        didSet{
            // Passwd Field design
            let cornerRAdius = 10
            vaidateButton.layer.cornerRadius = CGFloat(cornerRAdius)
            vaidateButton.layer.borderColor = UIColor.init(red: 1, green: 0.49, blue: 0, alpha: 1).cgColor
            vaidateButton.layer.borderWidth = 2
            vaidateButton.layer.masksToBounds = (cornerRAdius != 0)
        }
    }
    
    
    @IBOutlet weak var userNameView: UIView!{
        didSet{
            // Passwd Field design
            let cornerRAdius = 10
            userNameView.layer.cornerRadius = CGFloat(cornerRAdius)
            userNameView.layer.borderColor = UIColor.init(red: 1, green: 0.49, blue: 0, alpha: 1).cgColor
            userNameView.layer.borderWidth = 2
            userNameView.layer.masksToBounds = (cornerRAdius != 0)
        }
    }
    
    @IBOutlet weak var firstNameView: UIView!{
        didSet{
            // Passwd Field design
            let cornerRAdius = 10
            firstNameView.layer.cornerRadius = CGFloat(cornerRAdius)
            firstNameView.layer.borderColor = UIColor.init(red: 1, green: 0.49, blue: 0, alpha: 1).cgColor
            firstNameView.layer.borderWidth = 2
            firstNameView.layer.masksToBounds = (cornerRAdius != 0)
        }
    }
    
    @IBOutlet weak var lastNameView: UIView!{
        didSet{
            // Passwd Field design
            let cornerRAdius = 10
            lastNameView.layer.cornerRadius = CGFloat(cornerRAdius)
            lastNameView.layer.borderColor = UIColor.init(red: 1, green: 0.49, blue: 0, alpha: 1).cgColor
            lastNameView.layer.borderWidth = 2
            lastNameView.layer.masksToBounds = (cornerRAdius != 0)
        }
    }
    
    @IBOutlet weak var emailView: UIView!{
        didSet{
            // Passwd Field design
            let cornerRAdius = 10
            emailView.layer.cornerRadius = CGFloat(cornerRAdius)
            emailView.layer.borderColor = UIColor.init(red: 1, green: 0.49, blue: 0, alpha: 1).cgColor
            emailView.layer.borderWidth = 2
            emailView.layer.masksToBounds = (cornerRAdius != 0)
        }
    }
    
    
    @IBOutlet weak var passwordView: UIView!{
        didSet{
            // Passwd Field design
            let cornerRAdius = 10
            passwordView.layer.cornerRadius = CGFloat(cornerRAdius)
            passwordView.layer.borderColor = UIColor.init(red: 1, green: 0.49, blue: 0, alpha: 1).cgColor
            passwordView.layer.borderWidth = 2
            passwordView.layer.masksToBounds = (cornerRAdius != 0)
        }
    }
    
    @IBOutlet weak var verifyPasswdView: UIView!{
        didSet{
            // Passwd Field design
            let cornerRAdius = 10
            verifyPasswdView.layer.cornerRadius = CGFloat(cornerRAdius)
            verifyPasswdView.layer.borderColor = UIColor.init(red: 1, green: 0.49, blue: 0, alpha: 1).cgColor
            verifyPasswdView.layer.borderWidth = 2
            verifyPasswdView.layer.masksToBounds = (cornerRAdius != 0)
        }
    }
    
    // check mark for good fill
    @IBOutlet weak var userNameTick: UIImageView!{
        didSet{
            userNameTick.isHidden = true
            }
    }
    @IBOutlet weak var firstNameTick: UIImageView!{
        didSet{
            firstNameTick.isHidden = true
            }
    }
    @IBOutlet weak var lastNameTick: UIImageView!{
        didSet{
            lastNameTick.isHidden = true
            }
    }
    @IBOutlet weak var emailTick: UIImageView!{
        didSet{
            emailTick.isHidden = true
            }
    }
    @IBOutlet weak var passwdTick: UIImageView!{
        didSet{
            passwdTick.isHidden = true
            }
    }
    @IBOutlet weak var verifyPasswdTick: UIImageView!{
        didSet{
            verifyPasswdTick.isHidden = true
            }
    }
    
    // X mark for bad fill
    @IBOutlet weak var userNameXmark: UIImageView!{
        didSet{
            userNameXmark.isHidden = true
            }
    }
    @IBOutlet weak var firstNameXmark: UIImageView!{
        didSet{
            firstNameXmark.isHidden = true
            }
    }
    @IBOutlet weak var lastNameXmark: UIImageView!{
        didSet{
            lastNameXmark.isHidden = true
            }
    }
    @IBOutlet weak var emailXmark: UIImageView!{
        didSet{
            emailXmark.isHidden = true
            }
    }
    @IBOutlet weak var passwdXmark: UIImageView!{
        didSet{
            passwdXmark.isHidden = true
            }
    }
    @IBOutlet weak var verifyXmark: UIImageView!{
        didSet{
            verifyXmark.isHidden = true
            }
    }
    
    
    
    //MARK: - Action
    
    @IBAction func dismissKeyboard(_ sender: UITapGestureRecognizer) {
        UserNameField.resignFirstResponder()
        LastNameFied.resignFirstResponder()
        FirstNameField.resignFirstResponder()
        MailField.resignFirstResponder()
        PsswdField.resignFirstResponder()
        VerifyPasswdField.resignFirstResponder()
    }
    @IBAction func pseudoDidEnd(_ sender: UITextField) {
        if UserNameField.text == "" {
            vaidateButton.isEnabled = false
            userNameTick.isHidden = true
            userNameXmark.isHidden = false
        } else {
            vaidateButton.isEnabled = true
            userNameTick.isHidden = false
            userNameXmark.isHidden = true
        }
    }
    @IBAction func nameDidEnd(_ sender: UITextField) {
        if FirstNameField.text == "" {
            vaidateButton.isEnabled = false
            lastNameTick.isHidden = true
            lastNameXmark.isHidden = false
        } else {
            vaidateButton.isEnabled = true
            lastNameTick.isHidden = false
            lastNameXmark.isHidden = true
        }
    }
    @IBAction func firstNameDidEnd(_ sender: UITextField) {
        if LastNameFied.text == "" {
            vaidateButton.isEnabled = false
            firstNameTick.isHidden = true
            firstNameXmark.isHidden = false
        } else {
            vaidateButton.isEnabled = true
            firstNameTick.isHidden = false
            firstNameXmark.isHidden = true
        }
    }
    @IBAction func mailDidEnd(_ sender: UITextField) {
        if MailField.text == "" {
            vaidateButton.isEnabled = false
            emailTick.isHidden = true
            emailXmark.isHidden = false
        } else {
            vaidateButton.isEnabled = true
            emailTick.isHidden = false
            emailXmark.isHidden = true
        }
    }
    @IBAction func passwordDidEnd(_ sender: UITextField) {
        if PsswdField.text == "" {
            vaidateButton.isEnabled = false
            passwdTick.isHidden = true
            passwdXmark.isHidden = false
        } else {
            vaidateButton.isEnabled = true
            passwdTick.isHidden = false
            passwdXmark.isHidden = true
        }
    }
    @IBAction func verifyPasswdDidEnd(_ sender: UITextField) {
        if VerifyPasswdField.text == "" || VerifyPasswdField.text != PsswdField.text {
            displayAlert()
            vaidateButton.isEnabled = false
            verifyPasswdTick.isHidden = true
            verifyXmark.isHidden = false
        } else {
            vaidateButton.isEnabled = true
            verifyPasswdTick.isHidden = false
            verifyXmark.isHidden = true
            
        }
    }
    
    @IBAction func nextFormButton(_ sender: UIButton) {
    }
    
    
    
    
    
    
    // MARK: - Functions
    
    func displayAlert() {
        // Declare Alert message
        let dialogMessage = UIAlertController(title: "Erreur", message: "La confiramtion du mot de passe est différente du mot de passe.", preferredStyle: .alert)
        
        // Create OK button with action handler
        let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
        })
        
        //Add OK and Cancel button to dialog message
        dialogMessage.addAction(ok)
        // Present dialog message to user
        self.present(dialogMessage, animated: true, completion: nil)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
    if segue.destination is SeconcdFormController {
    let vc = segue.destination as? SeconcdFormController
        vc?.UserName = UserNameField.text
        vc?.FirstName = FirstNameField.text
        vc?.LastName = LastNameFied.text
        vc?.Password = PsswdField.text
        vc?.Mail = MailField.text
    }
    }
    
    
    //MARK: - ViewDidoad
    
    override func viewDidLoad() {
    super.viewDidLoad()

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MainFormController.backgroundTap))
        self.view.addGestureRecognizer(tapGestureRecognizer)
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(MainFormController.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(MainFormController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        UserNameField.delegate = self
        LastNameFied.delegate = self
        FirstNameField.delegate = self
        MailField.delegate = self
        PsswdField.delegate = self
        VerifyPasswdField.delegate = self
        
        
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            
            // if keyboard size is not available for some reason, dont do anything
           return
        }
        
        var shouldMoveViewUp = false
        
        // if active text field is not nil
        if let activeTextField = activeTextField {
            
            let bottomOfTextField = activeTextField.convert(activeTextField.bounds, to: self.view).maxY;
            let topOfKeyboard = self.view.frame.height - keyboardSize.height
            
            if bottomOfTextField > topOfKeyboard {
                shouldMoveViewUp = true
            }
        }
        
        if(shouldMoveViewUp) {
            self.view.frame.origin.y = 0 - keyboardSize.height
        }
    }
    @objc func keyboardWillHide(notification: NSNotification) {
        self.view.frame.origin.y = 0
    }
    
    @objc func backgroundTap(_ sender: UITapGestureRecognizer) {
        // go through all of the textfield inside the view, and end editing thus resigning first responder
        // ie. it will trigger a keyboardWillHide notification
        self.view.endEditing(true)
    }
}

    //MARK: - Extention (Delegate)

extension MainFormController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.activeTextField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.activeTextField = nil
    }

}
