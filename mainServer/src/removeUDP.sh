# UDP docker remover script
#!/bin/bash
# paja444

#/!\NOT working/!\

# Script Vars:
DIR_NAME="Lobby-$1"
DIR_LOBBIES="/home/paja444/yule_alpagame/mainServer/Lobbies/";
DIR_PATH="${DIR_PATH}${DIR_NAME}";

main() {
    # remove docker
    echo "remove docker";
    cd ${DIR_PATH};
    docker-compose down;
    # remove dir
    echo "remove dir";
    cd ${DIR_LOBBIES};
    rm -rf ${DIR_NAME};
    # close port
    echo "close port";
    sudo ufw delete allow $2/udp;
    echo "remove UDP container OK.";
}

