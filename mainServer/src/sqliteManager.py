## YULE PROJECT ##
## SQLite for test localy befor final MariaDB in OVH server ##
# Alexandre Pajak 08/06/2022

import sqlite3
from userManager import User
from datetime import date
from dateutil import parser


def initDB():  # !!! RUN ONY ONE !!!
    try:
        # create YuleDatabase.db file
        sqliteConnection = sqlite3.connect("YuleDatabase.db")
        cursor = sqliteConnection.cursor()
        print("Connected to YuleDatabase.db")

        # create Users Table
        cursor.execute(
            """CREATE TABLE IF NOT EXISTS Users
              (
                uuid              TEXT,
                userName          TEXT,
                firstName         TEXT,
                lastName          TEXT,
                password          TEXT,
                email             TEXT,
                birthDate         DATE    DEFAULT (1 / 1 / 2022),
                gender            TEXT,
                sexualOrientation TEXT,
                coupleState       TEXT,
                alcohol           TEXT,
                totalSips         INTEGER,
                isConnect         BOOLEAN,
                currentTCPAddress TEXT,
                currentUDPAddress TEXT
                )"""
        )
        print("init Users in YuleDatabase.db: OK")
        cursor.close()

    except sqlite3.Error as error:
        print("Error while connecting to sqlite", error)


def insertUser(user: User):
    try:
        sqliteConnection = sqlite3.connect("YuleDatabase.db")
        cursor = sqliteConnection.cursor()
        # Insert a row of data
        cursor.execute(
            f"""INSERT INTO Users VALUES (
                '{user.uuid}',
                '{user.userName}',
                '{user.firstName}',
                '{user.lastName}',
                '{user.password}',
                '{user.email}',
                '{user.birthDate}',
                '{user.gender}',
                '{user.sexualOrientation}',
                '{user.coupleState}',
                '{user.alcohol}',
                '{user.totalSips}',
                '{user.isConnect}',
                '{user.currentTCPAddress}',
                '{user.currentUDPAddress}'
                )"""
        )
        sqliteConnection.commit()
        cursor.close()

    except sqlite3.Error as error:
        print("Error while inserting User to sqlite", error)
    finally:
        if sqliteConnection:
            sqliteConnection.close()
            print("the sqlite connection is closed. Insert: OK")


def getUser(uuid: str) -> User:
    select = selectDBUser_AllColumn("uuid", uuid)[0]
    # print(f"****{select}****")
    # print(f"uuid -> {select[0]}")
    # print(f"userName -> {select[1]}")
    # print(f"firstname -> {select[2]}")
    # print(f"lastname -> {select[3]}")
    # print(f"passwd -> {select[4]}")
    # print(f"mail -> {select[5]}")
    # print(f"birth -> {select[6]}")
    # print(f"gender -> {select[7]}")
    # print(f"sexual -> {select[8]}")
    # print(f"couplstate -> {select[9]}")
    # print(f"alcool -> {select[10]}")
    # print(f"totalsip -> {select[11]}")
    # print(f"isconnect -> {select[12]}")
    # print(f"TCP -> {select[13]}")
    # print(f"UDP -> {select[14]}")
    user = User(
        uuid,
        select[1],
        select[2],
        select[3],
        select[4],
        select[5],
        parser.parse(select[6]),
        select[7],
        select[8],
        select[9],
        select[10],
        select[11],
        bool(select[12]),
        select[13],
        select[14],
    )
    return user


def printDBUsers():
    try:
        sqliteConnection = sqlite3.connect("YuleDatabase.db")
        cursor = sqliteConnection.cursor()
        print("Connected to YuleDatabase.db")

        cursor.execute("SELECT * FROM Users")

        rows = cursor.fetchall()
        if len(rows) == 0:
            print("No User in YuleDatabase.db")
        else:
            for row in rows:
                print(f"{row}\n")
            cursor.close
    except sqlite3.Error as error:
        print("Error while connecting to sqlite", error)
    finally:
        if sqliteConnection:
            sqliteConnection.close()
            print("the sqlite connection is closed")


def selectDBUser_OneColumn(column, where, equal) -> list:
    result = []
    try:
        sqliteConnection = sqlite3.connect("YuleDatabase.db")
        cursor = sqliteConnection.cursor()
        print("Connected to YuleDatabase.db")

        # Select single record now
        sql_select_query = f"""SELECT {column} from Users WHERE {where} = '{equal}'"""
        cursor.execute(sql_select_query)

        rows = cursor.fetchall()
        for row in rows:
            result.append(row)

        print("Record select successfully ")
        cursor.close()
        return result

    except sqlite3.Error as error:
        print("Failed to select record from sqlite table", error)
    finally:
        if sqliteConnection:
            sqliteConnection.close()
            print("the sqlite connection is closed")


def selectDBUser_AllColumn(where, equal) -> list:
    result = []
    try:
        sqliteConnection = sqlite3.connect("YuleDatabase.db")
        cursor = sqliteConnection.cursor()
        print("Connected to SQLite")

        # Select single record now
        sql_select_query = f"""SELECT * from Users WHERE {where} = '{equal}'"""
        cursor.execute(sql_select_query)
        rows = cursor.fetchall()
        for row in rows:
            print(row)
            result.append(row)
        print("Record select successfully ")
        cursor.close()
        return result

    except sqlite3.Error as error:
        print("Failed to select record from sqlite table", error)
    finally:
        if sqliteConnection:
            sqliteConnection.close()
            print("the sqlite connection is closed")


def selectDBUser_All() -> list:
    result = []
    try:
        sqliteConnection = sqlite3.connect("YuleDatabase.db")
        cursor = sqliteConnection.cursor()
        print("Connected to SQLite")

        # Select single record now
        sql_select_query = f"""SELECT * from Users"""
        cursor.execute(sql_select_query)
        rows = cursor.fetchall()
        for row in rows:
            print(row)
            result.append(row)
        print("Record select successfully ")
        cursor.close()
        return result

    except sqlite3.Error as error:
        print("Failed to select record from sqlite table", error)
    finally:
        if sqliteConnection:
            sqliteConnection.close()
            print("the sqlite connection is closed")


def removeDBUser(where, equal):
    try:
        sqliteConnection = sqlite3.connect("YuleDatabase.db")
        cursor = sqliteConnection.cursor()
        print("Connected to SQLite")

        # Deleting single record now
        sql_delete_query = f"""DELETE from Users where {where} = '{equal}'"""
        cursor.execute(sql_delete_query)
        sqliteConnection.commit()
        print("Record deleted successfully ")
        cursor.close()

    except sqlite3.Error as error:
        print("Failed to delete record from sqlite table", error)
    finally:
        if sqliteConnection:
            sqliteConnection.close()
            print("the sqlite connection is closed")


def updateDBUser(set, on, where, equal):
    try:
        sqliteConnection = sqlite3.connect("YuleDatabase.db")
        cursor = sqliteConnection.cursor()
        print("Connected to SQLite")

        # Updating single record now
        sql_update_query = (
            f"""UPDATE Users SET {set} = '{on}' WHERE {where} = '{equal}'"""
        )
        cursor.execute(sql_update_query)
        sqliteConnection.commit()
        print("Record updated successfully ")
        cursor.close()

    except sqlite3.Error as error:
        print("Failed to updated record from sqlite table", error)
    finally:
        if sqliteConnection:
            sqliteConnection.close()
            print("the sqlite connection is closed")


## TEST ###

# initDB()
# userTest = User("alex")
# userTest2 = User("toto")
# insertUser(userTest2)
# removeDBUser("uuid","4960a624-11e7-4fea-81de-b26b600e5626")
# updateDBUser('firstName','Pajak','userName','alex')
# print(selectDBUser_AllColumn("uuid","89727690-b649-47f8-a803-b69fa32c47b0"))
# print(selectDBUser_AllColumn("gender","UNKNOW"))
# print(selectDBUser_OneColumn("birthDate", "uuid", "89727690-b649-47f8-a803-b69fa32c47b0"))
# printDBUsers()
# print(selectDBUser_All())
