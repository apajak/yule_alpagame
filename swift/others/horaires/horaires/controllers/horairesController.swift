//
//  horairesController.swift
//  horaires
//
//  Created by PAJAK Alexandre on 18/06/2022.
//

import UIKit

class horairesController: UIViewController {
    var name = "NOM"
    var morning = true
    var morningArrive = Date.now
    var morningLeave = Date.now
    var eveningArrive = Date.now
    var eveningLeave = Date.now
    override func viewDidLoad() {
        super.viewDidLoad()
        nameabel.text = name
        arrvieDatePicker.timeZone = TimeZone.init(identifier: "UTC")
        leaveDatePicker.timeZone = TimeZone.init(identifier: "UTC")
        
    }
    // MARK: -Outlet
    @IBOutlet weak var nameabel: UILabel!
    @IBOutlet weak var MomentSelectorOutet: UISegmentedControl!
    @IBOutlet weak var arrvieDatePicker: UIDatePicker!
    
    @IBOutlet weak var leaveDatePicker: UIDatePicker!
    
    
    // MARK: -Action

    @IBAction func Momentselector(_ sender: UISegmentedControl) {
        if MomentSelectorOutet.selectedSegmentIndex == 0 {
            morning = true
        } else {
            morning = false
        }
    }
    
    @IBAction func sendData(_ sender: UIButton) {
        print(morning)
        print(morningArrive)
        print(morningLeave)
        print(eveningArrive)
        print(eveningLeave)
    }
    
    @IBAction func arriveTime(_ sender: UIDatePicker) {
        if morning == true {
            morningArrive = arrvieDatePicker.date
            print(morningArrive)
        } else {
            eveningArrive = arrvieDatePicker.date
            print(eveningArrive)
        }
    }
    @IBAction func leaveTime(_ sender: UIDatePicker) {
        if morning == true {
            morningLeave = leaveDatePicker.date
            print(morningArrive)
        } else {
            eveningLeave = leaveDatePicker.date
            print(eveningArrive)
        }
    }
}
