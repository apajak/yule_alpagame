//
//  CoreDataManager.swift
//  Yule
//
//  Created by PAJAK Alexandre on 01/07/2022.
//

import Foundation
import CoreData

func saveUser(userObject: UserObject) {
    // init instance
    let user = User(context: CoreDataStack.sharedInstance.viewContext)
    // make change
    user.password = userObject.password
    user.email = userObject.email
    user.birthDate = userObject.birthDate
    user.uuid = userObject.uuid
    user.firstName = userObject.firstName
    user.lastName = userObject.lastName
    user.isConnect = userObject.isConnect
    user.coupleState = userObject.coupleState
    user.sexualOrientation = userObject.sexualOrientation
    user.totalSips = Int32(userObject.totalSips)
    user.alcohol = userObject.alcohol
    user.userName = userObject.userName
    user.gender = userObject.gender
    // save
    do {
        try CoreDataStack.sharedInstance.viewContext.save()
        print("User Saved")
    } catch {
        print("CoreData unable to save \(user.email ?? "NoUser") infos.")
    }
    //check
    printCoreDataUsers()
}

func getUsersList()-> Array<User>{
    var usersList: [User] = []
    let request: NSFetchRequest<User> = User.fetchRequest()
    guard let users = try? CoreDataStack.sharedInstance.viewContext.fetch(request) else {return usersList}
    for user in users {
        usersList.append(user)
        }
    return usersList
}

func setAsCurrentUser(id:String)-> User{
    let usersList = getUsersList()
    var currentUser: User?
    for user in usersList {
        if user.email == id {
            print(user.uuid! + " set as current user")
            currentUser = user
    }
}
    return currentUser!
}

func userToObbjectConvertion(user: User)-> UserObject{
    let userObject = UserObject(uuid: user.uuid!, userName: user.userName!, firstName: user.firstName!, lastName: user.lastName!, password: user.password!, email: user.email!, birthDate: user.birthDate!, gender: user.gender!, sexualOrientation: user.sexualOrientation!, coupleState: user.coupleState!, alcohol: user.alcohol!, totalSips: Int(exactly: user.totalSips)!, isConnect: user.isConnect, currentTCPAddress: user.currentTCPAddress ?? "", currentUDPAddress: user.currentUDPAddress ?? "")
    return userObject
}

func deleteAllUser(){
    let request: NSFetchRequest<User> = User.fetchRequest()
    guard let users = try? CoreDataStack.sharedInstance.viewContext.fetch(request) else {return}
    for user in users {
        CoreDataStack.sharedInstance.viewContext.delete(user)
        // save
        do {
            try CoreDataStack.sharedInstance.viewContext.save()
            print("User removed")
        } catch {
            print("CoreData unable to remove")
        }
        
    }
}
func deleteOneUser(email:String){
    let request: NSFetchRequest<User> = User.fetchRequest()
    guard let users = try? CoreDataStack.sharedInstance.viewContext.fetch(request) else {return}
    for user in users {
        if user.email == email {
            CoreDataStack.sharedInstance.viewContext.delete(user)
            // save
            do {
                try CoreDataStack.sharedInstance.viewContext.save()
                print("User removed")
            } catch {
                print("CoreData unable to remove")
            }
        }
    }
}

func printCoreDataUsers(){
    let request: NSFetchRequest<User> = User.fetchRequest()
    guard let users = try? CoreDataStack.sharedInstance.viewContext.fetch(request) else {return}
    var usersTab: [String] = []
    for user in users {
        usersTab.append(user.userName ?? "ND")
        }
    print(usersTab)
    }
    

