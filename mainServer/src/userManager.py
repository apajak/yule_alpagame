## YULE PROJECT ##
## User class file ##
# Alexandre Pajak 07/06/2022
import uuid
import json
from typing import Any, List, TypeVar, Type, cast
from dataclasses import dataclass
from datetime import datetime, date
from typing import Any, TypeVar, Type, cast
import dateutil.parser


T = TypeVar("T")


def from_str(x: Any) -> str:
    assert isinstance(x, str)
    return x


def from_datetime(x: Any) -> date:
    return dateutil.parser.parse(x)


def from_int(x: Any) -> int:
    assert isinstance(x, int) and not isinstance(x, bool)
    return x


def from_bool(x: Any) -> bool:
    assert isinstance(x, bool)
    return x


def to_class(c: Type[T], x: Any) -> dict:
    assert isinstance(x, c)
    return cast(Any, x).to_dict()


@dataclass
class User:
    uuid: str
    userName: str
    firstName: str
    lastName: str
    password: str
    email: str
    birthDate: date
    gender: str
    sexualOrientation: str
    coupleState: str
    alcohol: str
    totalSips: int
    isConnect: bool
    currentTCPAddress: str
    currentUDPAddress: str

    @staticmethod
    def from_dict(obj: Any) -> "User":
        assert isinstance(obj, dict)
        uuid = from_str(obj.get("uuid"))
        userName = from_str(obj.get("userName"))
        firstName = from_str(obj.get("firstName"))
        lastName = from_str(obj.get("lastName"))
        password = from_str(obj.get("password"))
        email = from_str(obj.get("email"))
        birthDate = from_datetime(obj.get("birthDate"))
        gender = from_str(obj.get("gender"))
        sexualOrientation = from_str(obj.get("sexualOrientation"))
        coupleState = from_str(obj.get("coupleState"))
        alcohol = from_str(obj.get("alcohol"))
        totalSips = from_int(obj.get("totalSips"))
        isConnect = from_bool(obj.get("isConnect"))
        currentTCPAddress = from_str(obj.get("currentTCPAddress"))
        currentUDPAddress = from_str(obj.get("currentUDPAddress"))
        return User(
            uuid,
            userName,
            firstName,
            lastName,
            password,
            email,
            birthDate,
            gender,
            sexualOrientation,
            coupleState,
            alcohol,
            totalSips,
            isConnect,
            currentTCPAddress,
            currentUDPAddress,
        )

    def to_dict(self) -> dict:
        result: dict = {}
        result["uuid"] = from_str(self.uuid)
        result["userName"] = from_str(self.userName)
        result["firstName"] = from_str(self.firstName)
        result["lastName"] = from_str(self.lastName)
        result["password"] = from_str(self.password)
        result["email"] = from_str(self.email)
        result["birthDate"] = self.birthDate.isoformat()
        result["gender"] = from_str(self.gender)
        result["sexualOrientation"] = from_str(self.sexualOrientation)
        result["coupleState"] = from_str(self.coupleState)
        result["alcohol"] = from_str(self.alcohol)
        result["totalSips"] = from_int(self.totalSips)
        result["isConnect"] = from_bool(self.isConnect)
        result["currentTCPAddress"] = from_str(self.currentTCPAddress)
        result["currentUDPAddress"] = from_str(self.currentUDPAddress)
        return result

    def newUuid(self):
        self.uuid = str(uuid.uuid4())


def Userfromdict(s: Any) -> User:
    return User.from_dict(s)


def Usertodict(x: User) -> Any:
    return to_class(User, x)


def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""
    if isinstance(obj, (datetime, date)):
        return obj.isoformat()
    raise TypeError("Type %s not serializable" % type(obj))


def jsonParse_User(json_string: str) -> User:
    result = Userfromdict(json.loads(json_string))
    return result


def jsonDumps_User(user: User) -> str:
    userDict = Usertodict(user)
    print(userDict)
    return json.dumps(userDict, default=json_serial)


userList = []


def encode_client_address(str_client_address: str) -> bytes:
    ip = str_client_address.split(":")[0]
    port = str_client_address.split(":")[1]
    tupleAddress = (ip, int(port))
    return tupleAddress


def find_TCP_User(uuid: str) -> User:
    for user in userList:
        if user.uuid == uuid:
            return user
def add_TCP_User(user: User):
    userList.append(user)

def remove_TCP_User(uuid: str):
    for user in userList:
        if user.uuid == uuid:
            userList.remove(user)

## TEST JSON ##

# jsonstr = '{"uuid":"","coupleState":"Celibataire","firstName":"Pajak","birthDate":1998-11-10,"lastName":"Alexandre","currentSips":0,"isConnect":false,"userName":"Alex","password":"toto","sexualOrientation":"Femme","alcohol":"Beaucoup","totalSips":0,"email":"jakpa.sas@gmail.com","gender":"Homme"}'
# print(User.jsonParseUser(jsonstr))
# userTest = User(User.newUuid(), "alex")
# print(userTest.birthDate)
# jsonifyUser = userTest.toJson()
# print(f"jsonify User: {jsonifyUser}")
# userifyJson = User.jsonParseUser(jsonifyUser)
# print(f"userifyJson : {userifyJson}")
# print(userifyJson.uuid)
# print(userifyJson.userName)
# print(userifyJson.birthDate)
# date = userifyJson.birthDate
# print(type(date))