//
//  JsonManager.swift
//  Yule
//
//  Created by PAJAK Alexandre on 10/06/2022.
//

import Foundation
import CoreData

class User: NSManagedObject {
}

// MARK: - User class
class UserObject: Codable {
    let uuid: String
    let userName: String
    let firstName: String
    let lastName: String
    let password: String
    let email: String
    let birthDate: Date
    let gender: String
    let sexualOrientation: String
    let coupleState: String
    let alcohol: String
    let totalSips: Int
    let isConnect: Bool
    let currentTCPAddress: String
    let currentUDPAddress: String

    enum CodingKeys: String, CodingKey {
        case uuid = "uuid"
        case userName = "userName"
        case firstName = "firstName"
        case lastName = "lastName"
        case password = "password"
        case email = "email"
        case birthDate = "birthDate"
        case gender = "gender"
        case sexualOrientation = "sexualOrientation"
        case coupleState = "coupleState"
        case alcohol = "alcohol"
        case totalSips = "totalSips"
        case isConnect = "isConnect"
        case currentTCPAddress = "currentTCPAddress"
        case currentUDPAddress = "currentUDPAddress"
    }

    init(uuid: String, userName: String, firstName: String, lastName: String, password: String, email: String, birthDate: Date, gender: String, sexualOrientation: String, coupleState: String, alcohol: String, totalSips: Int, isConnect: Bool, currentTCPAddress: String, currentUDPAddress: String) {
        self.uuid = uuid
        self.userName = userName
        self.firstName = firstName
        self.lastName = lastName
        self.password = password
        self.email = email
        self.birthDate = birthDate
        self.gender = gender
        self.sexualOrientation = sexualOrientation
        self.coupleState = coupleState
        self.alcohol = alcohol
        self.totalSips = totalSips
        self.isConnect = isConnect
        self.currentTCPAddress = currentTCPAddress
        self.currentUDPAddress = currentUDPAddress
    }
}

func parseJSONUser(jsonString: String)-> UserObject?{
    print(jsonString)
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
    let data = jsonString.data(using: .utf8)!
    let decoder = JSONDecoder()
    decoder.dateDecodingStrategy = .formatted(formatter)
    let user = try! decoder.decode(UserObject.self,from:data)
    return user
}

func dumpJSONUser(user:UserObject)->String{
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
    let encoder = JSONEncoder()
    encoder.dateEncodingStrategy = .formatted(formatter)
    let jsonData = try! encoder.encode(user)
    let jsonifyUser = String(data: jsonData, encoding: .utf8)!
    return jsonifyUser
}
let formatter = DateFormatter()





