//
//  ConnectionController.swift
//  Yule
//
//  Created by PAJAK Alexandre on 10/06/2022.
//


import UIKit
import Network
import CoreData

class ConnectController: UIViewController {
    //MARK: - Gobal var
    //local
    //static let TCPclient = TCPClient.init(host: "127.0.0.1", port: 2004)
    //serverOVH
    static let TCPclient = TCPClient.init(host: "54.37.153.179", port: 2004)
    private var defaultUsers = getUsersList()
    static var CurrentUser:UserObject? = nil
    //MARK: - outlet
    @IBOutlet weak var connectIDView: UIView!{
        didSet{
            // Passwd Field design
            let cornerRAdius = 10
            connectIDView.layer.cornerRadius = CGFloat(cornerRAdius)
            connectIDView.layer.borderColor = UIColor.init(red: 1, green: 0.49, blue: 0, alpha: 1).cgColor
            connectIDView.layer.borderWidth = 2
            connectIDView.layer.masksToBounds = (cornerRAdius != 0)
        }
    }
    @IBOutlet weak var connectPsswdView: UIView!{
        didSet{
            // Passwd Field design
            let cornerRAdius = 10
            connectPsswdView.layer.cornerRadius = CGFloat(cornerRAdius)
            connectPsswdView.layer.borderColor = UIColor.init(red: 1, green: 0.49, blue: 0, alpha: 1).cgColor
            connectPsswdView.layer.borderWidth = 2
            connectPsswdView.layer.masksToBounds = (cornerRAdius != 0)
        }
    }
    @IBOutlet weak var connectIdTextfield: UITextField!{
        didSet {
            let orangePlaceholderText = NSAttributedString(string: "Email",
                                                        attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(red: 1, green: 0.49, blue: 0, alpha: 1)])
            
            connectIdTextfield.attributedPlaceholder = orangePlaceholderText
        }
    }
    @IBOutlet weak var connectButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var passwdTextField: UITextField!{
        didSet {
            let orangePlaceholderText = NSAttributedString(string: "Mot de passe",
                                                        attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(red: 1, green: 0.49, blue: 0, alpha: 1)])
            
            passwdTextField.attributedPlaceholder = orangePlaceholderText
        }
    }
    
    
    //MARK: - Action
    @IBAction func connectIdTextfieldDidEnd(_ sender: Any) {
        if connectIdTextfield.text == "" {
            connectButton.isEnabled = false
        } else {
            connectButton.isEnabled = true
        }
    }
    
    @IBAction func connectPasswdTextfieldDidend(_ sender: Any) {
        if passwdTextField.text == "" {
            connectButton.isEnabled = false
        } else {
            connectButton.isEnabled = true
        }
    }
        
    @IBAction func connectButtonDidPressed(_ sender: UIButton) {
        passwdTextField.isEnabled = false
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        let userId = connectIdTextfield.text
        let data = "*CONNECTAS*" + userId!
        let password = ConnectController.TCPclient.getResponse(data: data.data(using: .utf8)!)
        let inputPassword = self.passwdTextField.text!
        DispatchQueue.global(qos: .background).async {
            if compareHash(InputPassword: inputPassword , hash: password){
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                    print("Connection Accepted")
                    var isLocalUser = false
                    for user in self.defaultUsers {
                        if user.email == userId {
                            isLocalUser = true
                            break
                        }
                        print(isLocalUser)
                    }
                    if isLocalUser {
                        print("is Local user")
                        ConnectController.CurrentUser = userToObbjectConvertion(user: setAsCurrentUser(id: userId!))
                        self.performSegue(withIdentifier: "ConnectToHome", sender: nil)
                    } else {
                        print("is Not LOCAL user")
                        let userId = self.connectIdTextfield.text
                        let data = "*IMPORTUSER*" + userId!
                        let response = ConnectController.TCPclient.getResponse(data: data.data(using: .utf8)!)
                        let user = parseJSONUser(jsonString: response)
                        print("save")
                        saveUser(userObject: user!)
                        ConnectController.CurrentUser = userToObbjectConvertion(user: setAsCurrentUser(id: userId!))
                        self.performSegue(withIdentifier: "ConnectToHome", sender: nil)
                    }
                }
                
            }else {
                DispatchQueue.main.async {
                self.displayerrorPopUp()
                self.activityIndicator.stopAnimating()
                self.passwdTextField.isEnabled = true
                self.activityIndicator.isHidden = true
                print("Unable to Connect")
            }}
        }
        }
            
        
        
//        displayPasswordPopUp(pass: password)
    
    //MARK: - Functions
    func displayerrorPopUp() {
        let alertController = UIAlertController(title: "Mauvais Mot de passe",
                                                message: "Vous avez entrez le mauvais mot de passe.",
                                                preferredStyle: .alert)

        let cancelAction = UIAlertAction(title: "OK", style: .cancel) { _ in }
        let forgotPasswd = UIAlertAction(title: "Mot de passe oublié", style: .default,handler: { (action) -> Void in })
        alertController.addAction(cancelAction)
        alertController.addAction(forgotPasswd)

        // 3. Show
        present(alertController, animated: true, completion: nil)}

    
//     displayPasswordPopUp(pass: String) {
//
//    activityIndicator.isHidden = false
//    activityIndicator.startAnimating()
//    // 1. Create Alert
//    let alertController = UIAlertController(title: "Mot de passe",
//                                            message: "Entrez votre mot de passe.",
//                                            preferredStyle: .alert)
//
//    // 2. Create Actions
//    let confirmAction = UIAlertAction(title: "Valider", style: .default) { _ in
//        // 2a. Monitor the Text
//        if let txtField = alertController.textFields?.first, let inputPassword = txtField.text {
//            let userId = self.connectIdTextfield.text
//            if compareHash(InputPassword: inputPassword, hash: pass){
//                self.activityIndicator.stopAnimating()
//                print("Connection Accepted")
//                var isLocalUser = false
//                for user in self.defaultUsers {
//                    if user.email == userId {
//                        isLocalUser = true
//                        break
//                    }
//                    print(isLocalUser)
//                }
//                if isLocalUser {
//                    print("is Local user")
//                    ConnectController.CurrentUser = userToObbjectConvertion(user: setAsCurrentUser(id: userId!))
//                    self.performSegue(withIdentifier: "ConnectToHome", sender: nil)
//                } else {
//                    print("is Not LOCAL user")
//                    let userId = self.connectIdTextfield.text
//                    let data = "*IMPORTUSER*" + userId!
//                    let response = ConnectController.TCPclient.getResponse(data: data.data(using: .utf8)!)
//                    let user = parseJSONUser(jsonString: response)
//                    print("save")
//                    saveUser(userObject: user!)
//                    ConnectController.CurrentUser = userToObbjectConvertion(user: setAsCurrentUser(id: userId!))
//                    self.performSegue(withIdentifier: "ConnectToHome", sender: nil)
//                }
//
//
//            } else {
//                self.displayerrorPopUp()
//                self.activityIndicator.stopAnimating()
//                self.activityIndicator.isHidden = true
//                print("Unable to Connect")
//            }
//        }
//    }
//    let cancelAction = UIAlertAction(title: "Retour", style: .cancel) { _ in }
//    alertController.addTextField { textField in
//        textField.placeholder = "Mot de passe"
//    }
//    alertController.addAction(confirmAction)
//    alertController.addAction(cancelAction)
//
//    // 3. Show
//    present(alertController, animated: true, completion: nil)}
    
    //MARK: - ViewDidoadb
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapGesture = UITapGestureRecognizer(target: view, action: #selector(UIView.endEditing))
        view.addGestureRecognizer(tapGesture)
        activityIndicator.isHidden = true
        printCoreDataUsers()
        ConnectController.TCPclient.start()
        if (defaultUsers.count) > 0 {
            connectIdTextfield.text = defaultUsers[0].email
        }
        }
}

    //MARK: - Extention (Delegate)

extension ConnectController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    @objc func keyboardAppear(_ notification: Notification) {
            guard let frame = notification.userInfo?[UIViewController.keyboardFrameEndUserInfoKey] as? NSValue else { return }
            let keyboardFrame = frame.cgRectValue
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardFrame.height - 200
            }
        }

    @objc func keyboardDisappear(_ notification: Notification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
}
