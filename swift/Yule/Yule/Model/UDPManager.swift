//
//  UDPManager.swift
//  Yule
//
//  Created by PAJAK Alexandre on 12/08/2022.
//

import Foundation
// MARK: - Player
class UDP: Codable {
    var name: String
    var uuid: String
    var lobbyCode: String
    var ip: String
    var port: Int
    var master_uuid: String
    var master_username: String
    var client_number: Int
    var udp_client: [UserObject]

    enum CodingKeys: String, CodingKey {
        case name = "name"
        case uuid = "uuid"
        case lobbyCode = "lobbyCode"
        case ip = "ip"
        case port = "port"
        case master_uuid = "master_uuid"
        case master_username = "master_username"
        case client_number = "client_number"
        case udp_client = "udp_client"
    }

    init(name: String, uuid: String,lobbyCode: String, ip: String,port: Int,master_uuid:String,master_username:String,client_number:Int,udp_client: [UserObject]) {
        self.name = name
        self.uuid = uuid
        self.lobbyCode = lobbyCode
        self.ip = ip
        self.port = port
        self.master_uuid = master_uuid
        self.master_username = master_username
        self.client_number = client_number
        self.udp_client = udp_client
    }
}

func parseJSONUDP(jsonString: String)-> UDP?{
    print("JSONdata")
    print(jsonString)
    print("-----------------------------")
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
    let data = jsonString.data(using: .utf8)!
    let decoder = JSONDecoder()
    decoder.dateDecodingStrategy = .formatted(formatter)
    let udp = try! decoder.decode(UDP.self,from:data)
    return udp
}
func dumpJSONPUDP(udp:UDP)->String{
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
    let encoder = JSONEncoder()
    encoder.dateEncodingStrategy = .formatted(formatter)
    let jsonData = try! encoder.encode(udp)
    let jsonifyUdp = String(data: jsonData, encoding: .utf8)!
    return jsonifyUdp
}

