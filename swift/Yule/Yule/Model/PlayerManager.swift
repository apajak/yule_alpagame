//
//  PlayerManager.swift
//  Yule
//
//  Created by PAJAK Alexandre on 09/08/2022.
//

import Foundation
import CoreData


// MARK: - Player
class Player: Codable {
    let user: UserObject
    let currentSips: Int
    let isConnect: Bool

    enum CodingKeys: String, CodingKey {
        case user = "user"
        case currentSips = "currentSips"
        case isConnect = "isConnect"
    }

    init(user: UserObject, currentSips: Int, isConnect: Bool) {
        self.user = user
        self.currentSips = currentSips
        self.isConnect = isConnect
    }
}

func parseJSONPlayer(jsonString: String)-> Player?{
    print(jsonString)
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
    let data = jsonString.data(using: .utf8)!
    let decoder = JSONDecoder()
    decoder.dateDecodingStrategy = .formatted(formatter)
    let player = try! decoder.decode(Player.self,from:data)
    return player
}
func dumpJSONPlayer(user:Player)->String{
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
    let encoder = JSONEncoder()
    encoder.dateEncodingStrategy = .formatted(formatter)
    let jsonData = try! encoder.encode(user)
    let jsonifyPlayer = String(data: jsonData, encoding: .utf8)!
    return jsonifyPlayer
}

