//
//  BarbuController.swift
//  Yule
//
//  Created by PAJAK Alexandre on 09/08/2022.
//

import UIKit


class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var playerIcon: UIImageView!
    
    @IBOutlet weak var playerNameLabel: UILabel!
}


class BarbuController: UIViewController {
    
    //MARK: - Gobal var
    let currentUser = ConnectController.CurrentUser
    let data = "udpDATA"
    var imageName = "000.png"
    static var udp = UDP(name: "", uuid: "",lobbyCode: "", ip: "", port: 0, master_uuid: "", master_username: "", client_number: 0, udp_client: [])
    static var game = Barbu()
    var background = true
    //MARK: - outlet
    @IBOutlet weak var newCardButton: UIButton!
    @IBOutlet weak var cardImage: UIImageView!
    @IBOutlet weak var currentPlayerLabel: UILabel!
    @IBOutlet weak var cardDescriptionLabel: UILabel!
    @IBOutlet weak var gameRemainCardsLabel: UILabel!
    @IBOutlet weak var collectionview: UICollectionView!
    
    //pop up
    
    @IBOutlet weak var bluredBackground: UIView!
    @IBOutlet weak var popUpTake: UIView!
    @IBOutlet weak var rulesButton: UIButton!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var popUpTakeLabel: UILabel!
    
    
    
    //MARK: - Action
//    
    @IBAction func popUP_ContinueButtonDidTouch(_ sender: Any) {
        BarbuController.game.take = false
        let data = "*CLIENTUPDATEGAME*"+(dumpJSONBarbu(barbu: BarbuController.game))
        print(data)
        LobbyController.UDPclient.send(data: data.data(using: .utf8)!)
        usleep(5000)
        self.bluredBackground.isHidden = true
        self.popUpTake.isHidden = true
        
    }
    
    @IBAction func popUP_RulesButtonDidTouch(_ sender: Any) {
    }
    
    
    
    
    
    @IBAction func newCardButtonDidTouch(_ sender: Any) {
        let data = "*NEWCARD*"
        DispatchQueue.global(qos: .background).async {
            LobbyController.UDPclient.send(data: data.data(using: .utf8)!)
        }}
    //MARK: - Functions
    //MARK: - ViewDidoad
    
    override func viewDidLoad() {
           super.viewDidLoad()
           newCardButton.isHidden = true
           let data = "*GAME_CONNECTION*"+(currentUser?.uuid ?? "ND")
           LobbyController.UDPclient.send(data: data.data(using: .utf8)!)
           cardImage.image = UIImage(named: imageName)
           
           DispatchQueue.global(qos: .background).async {
               while self.background {
                   var response = UDPClient.serverResponse
                   if response.contains("*GAME_CONNECTION*"){
                       let tab = response.split(separator: Character("*"))
                       response = String(tab[1])
                       let game = parseJSONBarbu(jsonString: response)
                       UDPClient.serverResponse = ""
                       response = UDPClient.serverResponse
                       BarbuController.game = game!
                       DispatchQueue.main.async {
                           var allConnected = false
                           for player in game!.players {
                               if player.isConnect {
                                   allConnected = true
                               } else {
                                   allConnected = false
                                   break
                               }
                           }
                           if allConnected == true && self.currentUser?.uuid == BarbuController.game.players[0].user.uuid {
                               self.newCardButton.isHidden = false
                               self.cardDescriptionLabel.text = "Tirez une carte pour commancer le jeu !"
                           }

                       }
                       UDPClient.serverResponse = ""
                       }
                   
                   if response.contains("*SERVERUPDATEGAME*"){
                       let tab = response.split(separator: Character("*"))
                       response = String(tab[1])
                       BarbuController.game = parseJSONBarbu(jsonString: response)!
                   }
                   if response.contains("*NEWCARD*"){
                       let tab = response.split(separator: Character("*"))
                       response = String(tab[1])
                       let game = parseJSONBarbu(jsonString: response)
                       self.imageName = "\(game?.current_card ?? "000").png"
                       DispatchQueue.main.async {
                           self.cardImage.image = UIImage(named: self.imageName)
                           self.currentPlayerLabel.text = game?.players[game?.playerIndex ?? 0].user.userName
                           self.gameRemainCardsLabel.text = String(game?.cards[0].count ?? 0)
                           if game?.players[game!.playerIndex].user.uuid == self.currentUser?.uuid{
                               self.newCardButton.isHidden = false
                               let cornerRAdius = 10
                               self.newCardButton.layer.cornerRadius = CGFloat(cornerRAdius)
                               self.newCardButton.layer.masksToBounds = (cornerRAdius != 0)
                               self.newCardButton.layer.borderColor = UIColor.red.cgColor
                               self.newCardButton.layer.borderWidth = 3
                               if game?.take == true {
                                   self.bluredBackground.isHidden = false
                                   self.popUpTake.isHidden = false
                                   self.popUpTakeLabel.text = "\(String(describing: game?.value)) gorgées !"
                               }
                               // else wait
                           } else {
                               self.newCardButton.isHidden = true
                               self.newCardButton.layer.borderWidth = 0
                           }
                           if game?.cards[0].count == 0{
                               self.background = false
                               self.imageName = "000.png"
                               self.cardDescriptionLabel.text = "La Partie est términée"
                               self.cardImage.image = UIImage(named: self.imageName)
                               self.gameRemainCardsLabel.text = String(0)
                               self.newCardButton.isHidden = true
                               let targetVC = self.storyboard?.instantiateViewController(withIdentifier: "endgamecontroller") as! EndGameController
                               self.navigationController?.pushViewController(targetVC, animated: false)
                           }
                           }
                       }
                   usleep(500)
                }
           }
       }
   }
       //MARK: - Extention (Delegate)
extension BarbuController: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return LobbyController.udp.udp_client.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "userCell", for: indexPath) as! CollectionViewCell
        let user = LobbyController.udp.udp_client[indexPath.row]
        cell.playerNameLabel.text = user.userName
        
        
        return cell
    }
    

}
