## YULE PROJECT ##
## UDP class file ##
# Alexandre Pajak 08/05/2022

import json
import os
import uuid
from dataclasses import dataclass
from typing import Any, List, TypeVar, Type, cast, Callable

from userManager import User


UDPlist = []

T = TypeVar("T")


def from_str(x: Any) -> str:
    assert isinstance(x, str)
    return x


def from_int(x: Any) -> int:
    assert isinstance(x, int) and not isinstance(x, bool)
    return x


def from_list(f: Callable[[Any], T], x: Any) -> List[T]:
    assert isinstance(x, list)
    return [f(y) for y in x]


def to_class(c: Type[T], x: Any) -> dict:
    assert isinstance(x, c)
    return cast(Any, x).to_dict()


@dataclass
class UDP:
    name: str
    uuid: str
    lobbyCode: str
    ip: str
    port: int
    master_uuid: str
    master_username: str
    client_number: int
    udp_client: List[User]

    @staticmethod
    def from_dict(obj: Any) -> "UDP":
        assert isinstance(obj, dict)
        name = from_str(obj.get("name"))
        uuid = from_str(obj.get("uuid"))
        lobbyCode = from_str(obj.get("lobbyCode"))
        ip = from_str(obj.get("ip"))
        port = from_int(obj.get("port"))
        master_uuid = from_str(obj.get("master_uuid"))
        master_username = from_str(obj.get("master_username"))
        client_number = port = from_int(obj.get("client_number"))
        udp_client = from_list(User.from_dict, obj.get("udp_client"))
        return UDP(
            name,
            uuid,
            lobbyCode,
            ip,
            port,
            master_uuid,
            master_username,
            client_number,
            udp_client,
        )

    def to_dict(self) -> dict:
        result: dict = {}
        result["name"] = from_str(self.name)
        result["uuid"] = from_str(self.uuid)
        result["lobbyCode"] = from_str(self.lobbyCode)
        result["ip"] = from_str(self.ip)
        result["port"] = from_int(self.port)
        result["master_uuid"] = from_str(self.master_uuid)
        result["master_username"] = from_str(self.master_username)
        result["client_number"] = from_int(self.client_number)
        result["udp_client"] = from_list(lambda x: to_class(User, x), self.udp_client)

        return result

    def newUuid(self):
        self.uuid = str(uuid.uuid4())

    def newLobbyCode(self):
        # generate 8 only digit code
        self.lobbyCode = str(uuid.uuid4().int)[:8]
    def setLobbyCode(self):
        self.lobbyCode = str(uuid.uuid4().int)[:8]

################## JSON #####################
# Convert class.attribut to dict
def UDPaddressfromdict(s: Any) -> UDP:
    return UDP.from_dict(s)


# Parse Json for return Barbu Object
def jsonParse_UDP(json_string: str) -> UDP:
    result = UDPaddressfromdict(json.loads(json_string))
    return result


# Dumps Barbu oject to Json string
def jsonDumps_UDP(udp) -> str:
    UDPdict = udp.to_dict()
    return json.dumps(UDPdict)


def findGame(UDPlist: list, uuid: str) -> UDP:
    for udp in UDPlist:
        if udp.uuid == uuid:
            return udp

def cleanUP(udp_FileName):
    os.system(f'echo "stop UDP{udp_FileName}.."')
    os.system(f"rm {udp_FileName}")
    os.system(f'echo "delete {udp_FileName}"')
    os.system("exit")
    os.system(f'echo "UDP {udp_FileName} stoped sucessfully"')


##### For test (default server) ##### 54.37.153.179
try:
    udp = UDP("AlexDefaultLobby", "","12345678", "54.37.153.179", 20001, "", "", 0, [])
    udp.newUuid()
    UDPlist.append(udp)
    print(f"udp:{udp.lobbyCode} was added.")
except Exception:
    print("default UDP fail")
#####################################
