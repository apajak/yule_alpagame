# README (Local)

`note`: 
- On selfHosted version you can't run newUDP.sh and create UDP container in docker.
- you need create an default UDP_barbu and add it in UDPlist
- default Lobby as `12345678` join code
# Usage
- install Python's modules
    ```bash
    pip3 install -r requirements.txt
    ```
- open port `2004` as TCP
- open port `20001` as UDP

- On [main.py](./mainServer/src/main.py):
    - set `host` var as `"127.0.0.1"`
    - run
        ```bash
        (base) paja@paja-mac src % python3 main.py
        ```
- On [UdpManager.py](./mainServer/src/UDPManager.py): 
    -  For add default UDP server. uncomment:
        ```bash
        ##### For test (default server) ##### 54.37.153.179
        try:
            udp = UDP("AlexDefaultLobby", "","12345678", "127.0.0.1", 20001, "", "", 0, [])
            udp.newUuid()
            UDPlist.append(udp)
            print(f"udp:{udp.lobbyCode} was added.")
        except Exception:
            print("default UDP fail")
        #####################################
        ```
- run [UDPLocal.py](./mainServer/src/UDPLocal.py)
    ```bash
    (base) paja@paja-mac src % python3 UDPLocal.py
    ```

