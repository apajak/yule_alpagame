## YULE PROJECT ##
## TCP Python Server for phone <--> server communication ##
# Alexandre Pajak 18/05/2022

import socket
from _thread import *
from UDPManager import UDPlist, jsonDumps_UDP, jsonParse_UDP
from time import sleep
from sqliteManager import (
    getUser,
    insertUser,
    selectDBUser_OneColumn,
    updateDBUser,
)
from userManager import (
    jsonDumps_User,
    jsonParse_User,
    find_TCP_User,
    remove_TCP_User,
    add_TCP_User,
    userList,
)
import os

# Global Var:
ServerSideSocket = socket.socket()
#host = "127.0.0.1"
host = "54.37.153.179"
port = 2004
ThreadCount = 0
message = ""
print(UDPlist)

try:
    ServerSideSocket.bind((host, port))
except socket.error as e:
    print(str(e))
print(f"TCP server is listening at {host}:{port}")
ServerSideSocket.listen(5)


def TCPSendAll(data: str, connection):
    connection.sendall(str.encode(data))


# Main THREAD
def multi_threaded_client(connection):
    global ThreadCount
    # connection.send(str.encode(f"Server is working: {ThreadCount}"))
    while True:
        data = connection.recv(2048)
        print(f"data {data}")
        strData = data.decode("utf-8")
        print(f"data received: {strData}")

        # IF "*NEWUSER*" flag in tcp receive
        if strData.find("*NEWUSER*") == 0:
            strData = strData.split("*NEWUSER*")[1]
            newUser = jsonParse_User(strData)
            newUser.newUuid()
            try:
                insertUser(newUser)
                print(
                    f"SERVER: User ({newUser.uuid}/{newUser.email}) added succesfully"
                )
                # send back user data + uuid
                response = jsonDumps_User(newUser)
                connection.send(str.encode(response))

            except Exception:
                print("User add fail")
                response = "SERVER: User add fail"
                connection.send(str.encode(response))

        elif strData.find("*CONNECTAS*") == 0:
            print("*CONNECTAS*")
            strData = strData.split("*CONNECTAS*")[1]
            try:
                response = selectDBUser_OneColumn("password", "email", strData)[0][0]
                print(f"SERVER: user password found")
                connection.send(str.encode(response))
                userID = selectDBUser_OneColumn("uuid", "email", strData)[0][0]
                print(f"SERVER: user uuid found")
                updateDBUser("isConnect", "true", "uuid", userID)
                print("SERVER: user isConnect updated")
                user = getUser(userID)
                add_TCP_User(user)
                print(userList)
                print(f"SERVER: user added to TCP_User list")
                print(f"SERVER: user {user.userName} connected")

            except Exception:
                response = "SERVER: User not found"
                print(error)

        elif strData.find("*DISCONNECT*") == 0:
            print("*DISCONNECT*")
            strData = strData.split("*DISCONNECT*")[1]
            print(f"SERVER: user {strData} disconnected")
            try:
                remove_TCP_User(strData)
                print(f"SERVER: user removed from TCP_User list")
                response = "*DISCONNECT*OK"
                connection.send(str.encode(response))
                updateDBUser("isConnected", "false", "uuid", strData)
                print(f"SERVER: user disconnected from DB")

            except Exception:
                response = "SERVER: User not found"
                connection.send(str.encode(response))
                print(error)

        elif strData.find("*IMPORTUSER*") == 0:
            print("*IMPORTUSER*")
            strData = strData.split("*IMPORTUSER*")[1]
            try:
                id = selectDBUser_OneColumn("uuid", "email", strData)[0][0]
                print(f"id: {id}")
                user = getUser(id)
                print(f"user: {user}")
                try:
                    response = jsonDumps_User(user)
                    print(f"NONLOCALDATA {response}")
                except Exception:
                    print("error dumpUSER")
                print(f"SERVER: user data send")
                connection.send(str.encode(response))
            except Exception:
                response = "SERVER: User not found"
                print(error)

        # LOCAL TEST VERSION:
        # ----------------------------------------------#
        # elif strData.find("*NEW_UDP*") == 0:
        #     strData = strData.split("*NEW_UDP*")[1]
        #     print("*NEW_UDP*")
        #     try:
        #         # for run UDP Server automatically
        #         # os.system("python3 UDPserver_barbu.py")
        #         udp = jsonParse_UDP(strData)
        #         print(udp)
        #         udp.newUuid()
        #         udp.ip = "127.0.0.1"
        #         udp.port = 20001
        #         response = jsonDumps_UDP(udp)
        #         print(response)
        #         connection.send(str.encode(response))
        #         UDPlist.append(udp)
        #         print(UDPlist)
        #         print(f"Server UDP(Barbu) {udp.name} create succesfully")
        #     except Exception:
        #         response = "SERVER: UDP_Barbu error"
        #         connection.send(str.encode(response))
        #         print(error)
        # HOSTED TEST VERSION:
        # ----------------------------------------------#
        elif strData.find("*NEW_UDP*") == 0:
            strData = strData.split("*NEW_UDP*")[1]
            print("*NEW_UDP*")
            try:
                udp = jsonParse_UDP(strData)
                udp.ip = host
                udp.newUuid()
                udp.name = udp.uuid
                udp.newLobbyCode()
                print(f"uuid: {udp.uuid}")
                print(f"udp: {udp}")
                print(f"ip: {udp.ip}")
                print(f"LobbyCode: {udp.lobbyCode}")
                if len(UDPlist) > 0 and UDPlist[-1].port <= 65536:
                    udp.port = UDPlist[-1].port + 1
                else:
                    udp.port = 20001
                print(f"port: {udp.port}")
                response = jsonDumps_UDP(udp)
                print(f"response: {response}")
                UDPlist.append(udp)
                print(f"LISTE:->{UDPlist}")
                print(f"response: {response}")
                connection.send(str.encode(response))
                print("run newUDP")
                os.system(f"bash newUDP.sh {udp.uuid} {udp.ip} {udp.port} {udp.name}")
                print(f"Server UDP(Barbu) {udp.name} create succesfully")
                print(UDPlist)
            except Exception:
                response = "SERVER: UDP_Barbu error"
                connection.send(str.encode(response))
                print(error)
        # ----------------------------------------------#
        elif strData.find("*GET_LOBBY*") == 0:
            print("*GET UDP REQUEST *")
            try:
                response = ""
                print(f"UDPlist{UDPlist}")
                for udp in UDPlist:
                    if udp.lobbyCode == strData.split("*GET_LOBBY*")[1]:
                        response += f"{jsonDumps_UDP(udp)}"
                        print(f"response: {response}")
                        connection.send(str.encode(response))
            except Exception:
                response = "SERVER: UDP not found"
                connection.send(str.encode(response))
                print(error)

        elif strData.find("*REMOVE_UDP*") == 0:
            print("*removeUDP*")
            strData = strData.split("*removeUDP*")[1]
            try:
                for udp in UDPlist:
                    if udp.uuid == strData:
                        UDPlist.remove(udp)
                        os.system(f"bash removeUDP.sh {udp.uuid}")
                        print(f"UDP {udp.name} removed")
                        response = "SERVER: UDP removed"
                        connection.send(str.encode(response))
            except Exception:
                response = "SERVER: UDP remove error"
                connection.send(str.encode(response))
                print(error)
        else:
            response = "Server response: " + data.decode("utf-8")
            print(str(response))
        if not data:
            break
    connection.sendall(str.encode(response))

    ThreadCount -= 1
    connection.close()


while True:
    Client, address = ServerSideSocket.accept()
    print("Connected to: " + address[0] + ":" + str(address[1]))
    start_new_thread(multi_threaded_client, (Client,))
    ThreadCount += 1
    print("Thread Number: " + str(ThreadCount))
    # ServerSideSocket.close()
