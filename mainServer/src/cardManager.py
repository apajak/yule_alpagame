## YULE PROJECT ##
## ALL Functions for cards and decks managment ##
# Alexandre Pajak 05/08/2022

from random import randrange

# give a list with a specifique number of random cards for each player
def cards_create_deck(number_of_cards: int, players: int, jockers: False):
    colors = ["C", "D", "H", "S"]
    values = [
        "02",
        "03",
        "04",
        "05",
        "06",
        "07",
        "08",
        "09",
        "10",
        "11",
        "12",
        "13",
        "14",
    ]
    deck, hands = [], []
    for color in colors:
        for value in values:
            deck.append("{}{}".format(value, color))
    if jockers:
        deck.append("01J")
        deck.append("02J")
    for _ in range(players):
        hand = []
        while len(hand) < number_of_cards:
            hand.append(deck.pop(randrange(len(deck))))
        hands.append(hand)
    return hands
