import UIKit
import Foundation
class UserObject: Codable {
    let uuid: String
    let userName: String
    let firstName: String
    let lastName: String
    let password: String
    let email: String
    let birthDate: Date
    let gender: String
    let sexualOrientation: String
    let coupleState: String
    let alcohol: String
    let totalSips: Int
    let isConnect: Bool
    let currentTCPAddress: String
    let currentUDPAddress: String

    enum CodingKeys: String, CodingKey {
        case uuid = "uuid"
        case userName = "userName"
        case firstName = "firstName"
        case lastName = "lastName"
        case password = "password"
        case email = "email"
        case birthDate = "birthDate"
        case gender = "gender"
        case sexualOrientation = "sexualOrientation"
        case coupleState = "coupleState"
        case alcohol = "alcohol"
        case totalSips = "totalSips"
        case isConnect = "isConnect"
        case currentTCPAddress = "currentTCPAddress"
        case currentUDPAddress = "currentUDPAddress"
    }

    init(uuid: String, userName: String, firstName: String, lastName: String, password: String, email: String, birthDate: Date, gender: String, sexualOrientation: String, coupleState: String, alcohol: String, totalSips: Int, isConnect: Bool, currentTCPAddress: String, currentUDPAddress: String) {
        self.uuid = uuid
        self.userName = userName
        self.firstName = firstName
        self.lastName = lastName
        self.password = password
        self.email = email
        self.birthDate = birthDate
        self.gender = gender
        self.sexualOrientation = sexualOrientation
        self.coupleState = coupleState
        self.alcohol = alcohol
        self.totalSips = totalSips
        self.isConnect = isConnect
        self.currentTCPAddress = currentTCPAddress
        self.currentUDPAddress = currentUDPAddress
    }
}
let jsonStr = """
{"uuid": "6e3498a5-c9db-4120-9b1d-b5054071bdf2", "userName": "Alex", "firstName": "Alexandre", "lastName": "Pajak", "password": "$2a$10$nnHlbo8skGvq0WoQ0ziPA.GiePHCB/AECmIvBK4y0tgctSTXirp3.", "email": "jakpa.sas@gmail.com", "birthDate": "1998-11-10T13:13:23+00:00", "gender": "Homme", "sexualOrientation": "Femme", "coupleState": "Celibataire", "alcohol": "Beaucoup", "totalSips": 0, "isConnect": true, "currentTCPAddress": "ND", "currentUDPAddress": "127.0.0.1:65164"}
"""

func parseJSON(jsonString: String)-> UserObject?{
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
    let data = jsonString.data(using: .utf8)!
    let decoder = JSONDecoder()
    decoder.dateDecodingStrategy = .formatted(formatter)
    let user = try! decoder.decode(UserObject.self,from:data)
    return user
}
func parseJSONUser(jsonString: String)-> UserObject?{
    let data = jsonString.data(using: .utf8)!
    let decoder = JSONDecoder()
    decoder.dateDecodingStrategy = .iso8601
    let user = try! decoder.decode(UserObject.self,from:data)
    return user
}


let user = parseJSON(jsonString: jsonStr)
let user2 = parseJSONUser(jsonString: jsonStr)
print(user!)
print(user2!)
print(user!.birthDate)
print(user2!.birthDate)
print(user!.userName)
print(user2!.userName)
