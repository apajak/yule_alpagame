//
//  ViewController.swift
//  horaires
//
//  Created by PAJAK Alexandre on 18/06/2022.
//

import UIKit

class ViewController: UIViewController {
    
    var name: String = ""
    
    var morganeMorning : Date?
    var morganeEvening : Date?
    var morganeTotal : Date?
    
    var priscaMorning : Date?
    var priscaEvening : Date?
    var priscaTotal : Date?
    
    var coleenMorning : Date?
    var coleenEvening : Date?
    var coleenTotal : Date?
    
    var marciaMorning : Date?
    var marciaEvening : Date?
    var marciaTotal : Date?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    // MARK: -Outlet
    @IBOutlet weak var morningMorganeOutlet: UILabel!
    @IBOutlet weak var eveningMorganeOutlet: UILabel!
    @IBOutlet weak var totalMorganeOutlet: UILabel!
    
    @IBOutlet weak var morningPriscaOutlet: UILabel!
    @IBOutlet weak var eveningPriscaOutlet: UILabel!
    
    @IBOutlet weak var totaPriscaOutlet: UILabel!
    
    @IBOutlet weak var morningColeenOutlet: UILabel!
    @IBOutlet weak var eveningColeenOutlet: UILabel!
    @IBOutlet weak var totalColeenOutlet: UILabel!
    
    @IBOutlet weak var morningMarciaOutlet: UILabel!
    @IBOutlet weak var eveningMarciaOutlet: UILabel!
    
    @IBOutlet weak var totalMarciaOutlet: UILabel!
    
    
    
    // MARK: -Action
    @IBAction func MorganButton(_ sender: UIButton) {
        name = "Morgane"
    }
    @IBAction func PriscaButton(_ sender: UIButton) {
        name = "Prisca"
    }
    @IBAction func ColeenButton(_ sender: UIButton) {
        name = "Coleen"
    }
    @IBAction func MarciaButton(_ sender: UIButton) {
        name = "Marcia"
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
    if segue.destination is horairesController {
    let vc = segue.destination as? horairesController
    vc?.name = name
    }
    }

}

