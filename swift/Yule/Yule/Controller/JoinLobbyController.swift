//
//  JoinLobbyController.swift
//  Yule
//
//  Created by PAJAK Alexandre on 28/12/2022.
//

import Foundation
import UIKit

class JoinLobbyController: UIViewController {
    
    //MARK: - Gobal var
    var activeTextField : UITextField? = nil
    //MARK: - outlet

    @IBOutlet weak var joinButton: UIButton!
    @IBOutlet weak var lobbyCodeTextfield: UITextField!{
        didSet{
            // Passwd Field design
            let cornerRAdius = 10
            lobbyCodeTextfield.layer.cornerRadius = CGFloat(cornerRAdius)
            lobbyCodeTextfield.layer.borderColor = UIColor.white.cgColor
            lobbyCodeTextfield.layer.borderWidth = 2
            lobbyCodeTextfield.layer.masksToBounds = (cornerRAdius != 0)
            let orangePlaceholderText = NSAttributedString(string: "Code",
                                                        attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(red: 1, green: 0.49, blue: 0, alpha: 1)])
            
            lobbyCodeTextfield.attributedPlaceholder = orangePlaceholderText
        }
    }
    //MARK: - Action
    
    @IBAction func joinButtonDidTouch(_ sender: Any) {
    }
    
    @IBAction func lobbyCodetextfieldDidEnd(_ sender: Any) {
        if lobbyCodeTextfield.text == ""{
            print("le code est a 8 chiffres")
            joinButton.isEnabled = false
        } else if lobbyCodeTextfield.text?.count == 8 {
            joinButton.isEnabled = true
        }
    }
    
    //MARK: - Functions
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
    if segue.destination is LobbyController {
    let vc = segue.destination as? LobbyController
        vc?.code = lobbyCodeTextfield.text ?? ""
            }
    }

    //MARK: - ViewDidoad"
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MainFormController.backgroundTap))
        self.view.addGestureRecognizer(tapGestureRecognizer)
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(MainFormController.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(MainFormController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        lobbyCodeTextfield.delegate = self
   
        
        
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            
            // if keyboard size is not available for some reason, dont do anything
           return
        }
        
        var shouldMoveViewUp = false
        
        // if active text field is not nil
        if let activeTextField = activeTextField {
            
            let bottomOfTextField = activeTextField.convert(activeTextField.bounds, to: self.view).maxY;
            let topOfKeyboard = self.view.frame.height - keyboardSize.height
            
            if bottomOfTextField > topOfKeyboard {
                shouldMoveViewUp = true
            }
        }
        
        if(shouldMoveViewUp) {
            self.view.frame.origin.y = 0 - keyboardSize.height
        }
    }
    @objc func keyboardWillHide(notification: NSNotification) {
        self.view.frame.origin.y = 0
    }
    
    @objc func backgroundTap(_ sender: UITapGestureRecognizer) {
        // go through all of the textfield inside the view, and end editing thus resigning first responder
        // ie. it will trigger a keyboardWillHide notification
        self.view.endEditing(true)
    }
}

    //MARK: - Extention (Delegate)
extension JoinLobbyController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.activeTextField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.activeTextField = nil
    }

}
