import Foundation
class Barbu: Codable{
    let birthDate: Date
    enum CodingKeys: String, CodingKey {
        case birthDate = "birthDate"
    }
    init(birthDate: Date){
        self.birthDate = birthDate
    }
}


let jsonstr = "{\"birthDate\": \"1998-11-17T13:13:23+00:00\"}"

func parseJSONBarbu(jsonString: String)-> Barbu?{
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
    let data = jsonString.data(using: .utf8)!
    let decoder = JSONDecoder()
    decoder.dateDecodingStrategy = .formatted(formatter)
    let game = try! decoder.decode(Barbu.self,from:data)
    return game
}
func dumpJSONBarbu(barbu:Barbu)->String{
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
    let jsonifyBarbu : String
    let encoder = JSONEncoder()
    encoder.dateEncodingStrategy = .formatted(formatter)
    let jsonData = try! encoder.encode(barbu)
    jsonifyBarbu = String(data: jsonData, encoding: .utf8)!
    return jsonifyBarbu
}

let barbu = parseJSONBarbu(jsonString: jsonstr)
let jsons = dumpJSONBarbu(barbu: barbu!)
print(barbu!.birthDate)
print(jsons)


