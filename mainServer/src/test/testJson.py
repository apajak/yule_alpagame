from dataclasses import dataclass
from datetime import datetime, date
from locale import strcoll
from typing import Any, List, TypeVar, Type, cast, Callable
import dateutil.parser
from playerManager import Player, jsonDumps_Player,jsonParse_Player
import json
import uuid
from userManager import User, jsonParse_User, jsonDumps_User

T = TypeVar("T")


def from_str(x: Any) -> str:
    assert isinstance(x, str)
    return x


def from_datetime(x: Any) -> datetime:
    return dateutil.parser.parse(x)


def from_int(x: Any) -> int:
    assert isinstance(x, int) and not isinstance(x, bool)
    return x


def from_bool(x: Any) -> bool:
    assert isinstance(x, bool)
    return x


def to_class(c: Type[T], x: Any) -> dict:
    assert isinstance(x, c)
    return cast(Any, x).to_dict()


def from_list(f: Callable[[Any], T], x: Any) -> List[T]:
    assert isinstance(x, list)
    return [f(y) for y in x]


@dataclass
class Barbu:
    # game properties
    players: List[Player]
    cards: List[List[str]]
    current_card: str
    playerIndex: int
    uuid: str
    gameMaster_uuid: str

    # game user data properties
    soberList: List[Player] 
    lowDrunkList: List[Player]
    midleDrunkList: List[Player]
    highDrunkList: List[Player]
    coupleList: List[Player] 
    singleList: List[Player] 
    maleList: List[Player]
    femaleList: List[Player]

    # cards properties
    whores: List[Player] 
    pimp: Player
    shiFuMi: bool
    iNever: bool
    withoutSC: bool 
    iHave: bool 
    inMyCase: bool 
    BtB: bool
    Valkyries: bool 
    kingOfIce: Player

    def newUuid(self):
        self.uuid = str(uuid.uuid4())

    @staticmethod
    def from_dict(obj: Any) -> "Barbu":
        assert isinstance(obj, dict)

        players = from_list(Player.from_dict, obj.get("players"))
        cards = from_list(lambda x: from_list(from_str, x), obj.get("cards"))
        current_card = from_str(obj.get("current_card"))
        playerIndex = from_int(obj.get("playerIndex"))
        uuid = from_str(obj.get("uuid"))
        gameMaster_uuid = from_str(obj.get("gameMaster_uuid"))

        soberList = from_list(Player.from_dict, obj.get("soberList"))
        lowDrunkList = from_list(Player.from_dict, obj.get("lowDrunkList"))
        midleDrunkList = from_list(Player.from_dict, obj.get("midleDrunkList"))
        highDrunkList = from_list(Player.from_dict, obj.get("highDrunkList"))
        coupleList = from_list(Player.from_dict, obj.get("coupleList"))
        singleList = from_list(Player.from_dict, obj.get("singleList"))
        maleList = from_list(Player.from_dict, obj.get("maleList"))
        femaleList = from_list(Player.from_dict, obj.get("femaleList"))

        whores = from_list(Player.from_dict, obj.get("whores"))
        pimp = Player.from_dict(obj.get("pimp"))
        shiFuMi = from_bool(obj.get("shiFuMi"))
        iNever = from_bool(obj.get("iNever"))
        withoutSC = from_bool(obj.get("withoutSC"))
        iHave = from_bool(obj.get("iHave"))
        inMyCase = from_bool(obj.get("inMyCase"))
        BtB = from_bool(obj.get("BtB"))
        Valkyries = from_bool(obj.get("Valkyries"))
        kingOfIce = Player.from_dict(obj.get("kingOfIce"))

        return Barbu(
            players,
            cards,
            current_card,
            playerIndex,
            uuid,
            gameMaster_uuid,
            soberList,
            lowDrunkList,
            midleDrunkList,
            highDrunkList,
            coupleList,
            singleList,
            maleList,
            femaleList,
            whores,
            pimp,
            shiFuMi,
            iNever,
            withoutSC,
            iHave,
            inMyCase,
            BtB,
            Valkyries,
            kingOfIce,
        )

    def to_dict(self) -> dict:
        result: dict = {}
        result["players"] = from_list(lambda x: to_class(Player, x), self.players)
        result["cards"] = from_list(lambda x: from_list(from_str, x), self.cards)
        result["current_card"] = from_str(self.current_card)
        result["playerIndex"] = from_int(self.playerIndex)
        result["uuid"] = from_str(self.uuid)
        result["gameMaster_uuid"] = from_str(self.gameMaster_uuid)

        result["soberList"] = from_list(lambda x: to_class(Player, x), self.soberList)
        result["lowDrunkList"] = from_list(
            lambda x: to_class(Player, x), self.lowDrunkList
        )
        result["midleDrunkList"] = from_list(
            lambda x: to_class(Player, x), self.midleDrunkList
        )
        result["highDrunkList"] = from_list(
            lambda x: to_class(Player, x), self.highDrunkList
        )
        result["coupleList"] = from_list(lambda x: to_class(Player, x), self.coupleList)
        result["singleList"] = from_list(lambda x: to_class(Player, x), self.singleList)
        result["maleList"] = from_list(lambda x: to_class(Player, x), self.maleList)
        result["femaleList"] = from_list(lambda x: to_class(Player, x), self.femaleList)

        result["whores"] = from_list(lambda x: to_class(Player, x), self.whores)
        result["pimp"] = from_list(lambda x: to_class(Player, x), self.pimp)
        result["shiFuMi"] = from_bool(self.shiFuMi)
        result["iNever"] = from_bool(self.iNever)
        result["withoutSC"] = from_bool(self.withoutSC)
        result["iHave"] = from_bool(self.iHave)
        result["inMyCase"] = from_bool(self.inMyCase)
        result["BtB"] = from_bool(self.BtB)
        result["Valkyries"] = from_bool(self.Valkyries)
        result["kingOfIce"] = from_list(lambda x: to_class(Player, x), self.kingOfIce)
        return result

    # method for increment playerIndex
    def next_Player(self):
        numberOfPlayers = len(self.players)
        if self.playerIndex + 1 < numberOfPlayers:
            self.playerIndex += 1
        elif self.playerIndex + 1 == numberOfPlayers:
            self.playerIndex = 0


################## JSON #####################
# Convert class.attribut to dict
def Barbufromdict(s: Any) -> Barbu:
    return Barbu.from_dict(s)


# Parse Json for return Barbu Object
def jsonParse_Barbu(json_string: str) -> Barbu:
    result = Barbufromdict(json.loads(json_string))
    return result


# Resolv Date Conflict (convert Datetime Object to iso8601 string)
def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""
    if isinstance(obj, (datetime, date)):
        return obj.isoformat()
    raise TypeError("Type %s not serializable" % type(obj))


# Dumps Barbu oject to Json string
def jsonDumps_Barbu(game: Barbu) -> str:
    barbuDict = Barbu.to_dict(game)
    return json.dumps(barbuDict, default=json_serial)


# Dumps Barbu oject to Json string
user1 = User("","alex","","","","",date.today,"","","","",0,False,"","")
user2 = User("","pa","","","","",date.today,"","","","",0,False,"","")
# player1 = Player(user1,0,False)
# player2 = Player(user2,0,False)
# barbu = Barbu([player1],[[]],"000.png",0,"uuid","uuid",[player1,player2],[player1,player2],[player1,player2],[player1,player2],[player1,player2],[player1,player2],[player1,player2],[player1,player2],[player1,player2],player1,False,False,False,False,False,False,False,player1)
# print(jsonDumps_Barbu(barbu))

user1 = jsonParse_User('{"uuid":"b5c18a5f-4251-4c74-a908-9ce6563ebd0f","coupleState":"Celibataire","firstName":"Pierre-Alain","birthDate":"1998-11-17T13:13:23Z","lastName":"Lorrain","isConnect":true,"userName":"PA","password":"$2a$10$vm4xQq61RBaLzGLPGlS\/LOtRvSuEcCzWLahhFpJ04o6idk98vy\/FO","sexualOrientation":"Femme","alcohol":"Beaucoup","currentUDPAddress":"","totalSips":0,"currentTCPAddress":"","email":"pa@gmail.com","gender":"Homme"}')
user2 = jsonParse_User('{"uuid":"b5c18a5f-4251-4c74-a908-9ce6563ebd0f","coupleState":"Celibataire","firstName":"Alexandre","birthDate":"1998-11-17T13:13:23Z","lastName":"Lorrain","isConnect":true,"userName":"PA","password":"$2a$10$vm4xQq61RBaLzGLPGlS\/LOtRvSuEcCzWLahhFpJ04o6idk98vy\/FO","sexualOrientation":"Femme","alcohol":"Beaucoup","currentUDPAddress":"","totalSips":0,"currentTCPAddress":"","email":"pa@gmail.com","gender":"Homme"}')
player1 = Player(user1,0,False)
player2 = Player(user2,0,False)
jsons = jsonDumps_Player(player1)
jsonParse_Player(jsons)

barbu = Barbu([],[[]],"000.png",0,"uuid","uuid",[],[],[],[],[],[],[],[],[],[],False,False,False,False,False,False,False,[])

print(jsonDumps_Barbu(barbu))

