## YULE PROJECT ##
## Player class file ##
# Alexandre Pajak 08/05/2022

import datetime
import json
from datetime import date
from json import JSONEncoder, dumps, loads
from userManager import User
from dataclasses import dataclass
from datetime import datetime
from typing import Any, TypeVar, Type, cast
import dateutil.parser


T = TypeVar("T")


def from_str(x: Any) -> str:
    assert isinstance(x, str)
    return x


def from_datetime(x: Any) -> datetime:
    return dateutil.parser.parse(x)


def from_int(x: Any) -> int:
    assert isinstance(x, int) and not isinstance(x, bool)
    return x


def from_bool(x: Any) -> bool:
    assert isinstance(x, bool)
    return x


def to_class(c: Type[T], x: Any) -> dict:
    assert isinstance(x, c)
    return cast(Any, x).to_dict()


@dataclass
class Player:
    user: User
    currentSips: int
    isConnect: bool

    @staticmethod
    def from_dict(obj: Any) -> "Player":
        assert isinstance(obj, dict)
        user = User.from_dict(obj.get("user"))
        currentSips = from_int(obj.get("currentSips"))
        isConnect = from_bool(obj.get("isConnect"))
        return Player(user, currentSips, isConnect)

    def to_dict(self) -> dict:
        result: dict = {}
        result["user"] = to_class(User, self.user)
        result["currentSips"] = from_int(self.currentSips)
        result["isConnect"] = from_bool(self.isConnect)
        return result


def Playerfromdict(s: Any) -> Player:
    return Player.from_dict(s)


def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""
    if isinstance(obj, (datetime, date)):
        return obj.isoformat()
    raise TypeError("Type %s not serializable" % type(obj))


def jsonParse_Player(json_string: str) -> Player:
    result = Playerfromdict(json.loads(json_string))
    print(result.user.birthDate)
    return result


def jsonDumps_Player(player: Player) -> str:
    playerDict = Player.to_dict(player)
    print(playerDict)
    return json.dumps(playerDict, default=json_serial)


def findPlayer(playerlist: list, uuid: str) -> Player:
    for player in playerlist:
        if player.user.uuid == uuid:
            return player


###### TEST: ######
# strJson = '{"user": {"uuid": "7a869e59-22d5-4af8-a632-570b3eb015b5", "userName": "Alex", "firstName": "alexandre", "lastName": "Pajak", "password": "$2a$10$vj9CAq03e94O9CjqP45IxOpluWw8MyEX0AwqrvHOIKWd2/KRQg5B2", "email": "jakpa.sas@gmail.com", "birthDate": "1998-11-10T00:00:00", "gender": "Homme", "sexualOrientation": "Femme", "coupleState": "Celibataire", "alcohol": "Beaucoup", "totalSips": 0, "isConnect": false, "currentTCPAddress": "127.0.0.1:52735", "currentUDPAddress": "127.0.0.1:52735"},"currentSips": 0,"isConnect": false}'
# player = jsonParse_Player(strJson)
# print(player.user.birthDate)
# jsonify = jsonDumps_Player(player)
# print(jsonify)
