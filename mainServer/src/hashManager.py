## YULE PROJECT ##
## hash file ##
# Alexandre Pajak 07/06/2022


import bcrypt


def hashPassword(passwd: str):

    return bcrypt.hashpw(passwd.encode(), bcrypt.gensalt())


def checkPassword(passwd: str, hash) -> bool:
    return bcrypt.checkpw(passwd.encode(), hash.encode())
