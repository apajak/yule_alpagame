//
//  SecondFormController.swift
//  Yule
//
//  Created by PAJAK Alexandre on 15/06/2022.
//

import UIKit
import Network
import CoreData

class SeconcdFormController: UIViewController {
    
    //MARK: - Gobal var
    var UserName : String? = ""
    var LastName : String? = ""
    var FirstName : String? = ""
    var Mail : String? = ""
    var Password : String? = ""
    var BirthDate : Date? = nil
    var Gender : String? = "ND"
    var SexualOrientation : String? = "ND"
    var CoupleState : String? = "ND"
    var Alcohol : String? = "Moderer"
    
    let orange = UIColor.init(red: 1, green: 0.49, blue: 0, alpha: 1)
    let MINIMUM_AGE: Date = Calendar.current.date(byAdding: .year, value: -18, to: Date())!;
    
    //MARK: - outlet

    @IBOutlet weak var validateButton: UIButton!
    @IBOutlet weak var DatePicker: UIDatePicker!{
        didSet{
            DatePicker.setValue(orange, forKeyPath: "textColor" )
        }
    }
    
    @IBOutlet weak var GenderControl: UISegmentedControl!{
        didSet{
            GenderControl.frame.size.height = 50.0
        }
    }
    @IBOutlet weak var sexualOrientationControl: UISegmentedControl!{
        didSet{
            sexualOrientationControl.frame.size.height = 50.0
        }
    }
    
    @IBOutlet weak var CoupleStateControl: UISegmentedControl!{
        didSet{
            CoupleStateControl.frame.size.height = 50.0
        }
    }
    @IBOutlet weak var AlcoholControl: UISegmentedControl!{
        didSet{
            AlcoholControl.frame.size.height = 50.0
        }
    }
    
    @IBOutlet weak var birthDateView: UIView!{
        didSet{
            // Passwd Field design
            let cornerRAdius = 10
            birthDateView.layer.cornerRadius = CGFloat(cornerRAdius)
            birthDateView.layer.borderColor = UIColor.init(red: 1, green: 0.49, blue: 0, alpha: 1).cgColor
            birthDateView.layer.borderWidth = 2
            birthDateView.layer.masksToBounds = (cornerRAdius != 0)
        }
    }
    
    @IBOutlet weak var genderView: UIView!{
        didSet{
            // Passwd Field design
            let cornerRAdius = 10
            genderView.layer.cornerRadius = CGFloat(cornerRAdius)
            genderView.layer.borderColor = UIColor.init(red: 1, green: 0.49, blue: 0, alpha: 1).cgColor
            genderView.layer.borderWidth = 2
            genderView.layer.masksToBounds = (cornerRAdius != 0)
        }
    }
    @IBOutlet weak var genderInfoButton: UIButton!
    @IBOutlet weak var coupleStateView: UIView!{
        didSet{
            // Passwd Field design
            let cornerRAdius = 10
            coupleStateView.layer.cornerRadius = CGFloat(cornerRAdius)
            coupleStateView.layer.borderColor = UIColor.init(red: 1, green: 0.49, blue: 0, alpha: 1).cgColor
            coupleStateView.layer.borderWidth = 2
            coupleStateView.layer.masksToBounds = (cornerRAdius != 0)
        }
    }
    @IBOutlet weak var coupleStateInfoButton: UIButton!
    @IBOutlet weak var sexualPreferenceView: UIView!{
        didSet{
            // Passwd Field design
            let cornerRAdius = 10
            sexualPreferenceView.layer.cornerRadius = CGFloat(cornerRAdius)
            sexualPreferenceView.layer.borderColor = UIColor.init(red: 1, green: 0.49, blue: 0, alpha: 1).cgColor
            sexualPreferenceView.layer.borderWidth = 2
            sexualPreferenceView.layer.masksToBounds = (cornerRAdius != 0)
        }
    }
    @IBOutlet weak var sexualPreferenceInfoButton: UIButton!
    @IBOutlet weak var alcoolPreferenceView: UIView!{
        didSet{
            // Passwd Field design
            let cornerRAdius = 10
            alcoolPreferenceView.layer.cornerRadius = CGFloat(cornerRAdius)
            alcoolPreferenceView.layer.borderColor = UIColor.init(red: 1, green: 0.49, blue: 0, alpha: 1).cgColor
            alcoolPreferenceView.layer.borderWidth = 2
            alcoolPreferenceView.layer.masksToBounds = (cornerRAdius != 0)
        }
    }
    @IBOutlet weak var alcoolPreferenceButton: UIButton!
    
    
    
    
    
    
    
    
    
    
    
    
    
    //MARK: - Action
    @IBAction func validateButtonTouchUp(_ sender: Any) {
        if (UserName != nil) && (FirstName != nil) && (LastName != nil) && (Password != nil) && (Mail != nil) && (BirthDate != nil) && Alcohol != nil {
            // Save to Core Data
            let hashPasswd = createHash(InputPassword: Password!)
            let newUser = UserObject(uuid: "ND", userName: UserName!, firstName: FirstName!, lastName: LastName!, password: hashPasswd, email: Mail!, birthDate: BirthDate!, gender: Gender!, sexualOrientation: SexualOrientation!, coupleState: CoupleState!, alcohol: Alcohol!, totalSips: 0,  isConnect: false, currentTCPAddress: "ND", currentUDPAddress: "ND")
            let newUserJson = "*NEWUSER*" + dumpJSONUser(user: newUser)
            print(newUserJson)
            let response = ConnectController.TCPclient.getResponse(data: newUserJson.data(using: .utf8)!)
            print("response: \(response)")
            let user = parseJSONUser(jsonString: response)!
            saveUser(userObject: user)
        } else {
            print("error Data")
        }
    }
    
    @IBAction func DatePickerDidEnd(_ sender: UIDatePicker) {
        let isValidAge = validateAge(birthDate: DatePicker.date)
        if isValidAge {
            BirthDate = DatePicker.date
            print(BirthDate!)
            validateButton.isEnabled = true
        } else {
            displayBirthDateAlert()
            validateButton.isEnabled = false
        }
    }
    @IBAction func genderControlDidEnd(_ sender: UISegmentedControl) {
        switch GenderControl.selectedSegmentIndex {
         case 0:
             Gender = "Femme"
         case 1:
             Gender = "ND"
         case 2:
             Gender = "Homme"
        default:
            print("empty Gender")
        }
    }
    @IBAction func coupleStateControlDidEnd(_ sender: UISegmentedControl) {
        switch CoupleStateControl.selectedSegmentIndex {
         case 0:
             CoupleState = "Celibataire"
         case 1:
             CoupleState = "ND"
         case 2:
             CoupleState = "Couple"
        default:
            print("empty Couple State")
        }
    }
    @IBAction func sexualOrientationControlDidEnd(_ sender: UISegmentedControl) {
        switch sexualOrientationControl.selectedSegmentIndex {
         case 0:
             SexualOrientation = "Femme"
         case 1:
            SexualOrientation = "ND"
         case 2:
            SexualOrientation = "Homme"
        default:
            print("empty Sexual Orientation")
        }
    }
    @IBAction func alcoholControlDidEnd(_ sender: UISegmentedControl) {
        switch AlcoholControl.selectedSegmentIndex {
         case 0:
            Alcohol = "NO"
         case 1:
             Alcohol = "LOW"
         case 2:
            Alcohol = "MIDLE"
         case 3:
            Alcohol = "HIGH"
        default:
            print("empty Alcohol State")
        }
    }
    
    // info buttons
    @IBAction func genderButtonDidTouch(_ sender: Any) {
        displayGenderAlert()
    }
    @IBAction func coupleStateButtonDidTouch(_ sender: Any) {
        displaycoupleStateAlert()
    }
    @IBAction func sexualPreferenceButtonDidTouch(_ sender: Any) {
        displaysexualPreferenceAlert()
    }
    @IBAction func alcoolPreferenceButtonDidTouch(_ sender: Any) {
        displayAlcoolAlert()
    }
    
    
    
    
    // MARK: - functions
    
    func displayBirthDateAlert() {
        // Declare Alert message
        let dialogMessage = UIAlertController(title: "Erreur", message: "Vous devez être majeur pour créer un compte.", preferredStyle: .alert)
        
        // Create OK button with action handler
        let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
        })
    
        
        //Add OK and Cancel button to dialog message
        dialogMessage.addAction(ok)
        
        // Present dialog message to user
        self.present(dialogMessage, animated: true, completion: nil)
        
    }
    
    func validateAge(birthDate: Date) -> Bool {
        var isValid: Bool = true;
        
        if  birthDate > MINIMUM_AGE {
            isValid = false;
        }
        
        return isValid;
    }
    
    func displayGenderAlert() {
        // Declare Alert message
        let dialogMessage = UIAlertController(title: "Information genre", message: "Nous vous proposons de renseigner votre genre afin d'affiner les questions durant vos futures parties. Fini les skip de questions gênante ! 😌\n\nCette information n'est pas visible des autres joueurs.", preferredStyle: .alert)
        
        // Create OK button with action handler
        let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
        })
    
        
        //Add OK and Cancel button to dialog message
        dialogMessage.addAction(ok)
        
        // Present dialog message to user
        self.present(dialogMessage, animated: true, completion: nil)
        
    }
    func displaycoupleStateAlert() {
        // Declare Alert message
        let dialogMessage = UIAlertController(title: "Information couple", message: "Nous vous proposons de renseigner votre status de couple afin de ne rien vous proposer de compromettant. 😉\n\nCette information n'est pas visible des autres joueurs.", preferredStyle: .alert)
        
        // Create OK button with action handler
        let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
        })
    
        
        //Add OK and Cancel button to dialog message
        dialogMessage.addAction(ok)
        
        // Present dialog message to user
        self.present(dialogMessage, animated: true, completion: nil)
    }
    func displaysexualPreferenceAlert() {
        // Declare Alert message
        let dialogMessage = UIAlertController(title: "Information Attirence", message: "Nous vous proposons de renseigner quel genre vous attire afin de proposer des questions plus pertinentes 😉\n\nCette information n'est pas visible des autres joueurs.", preferredStyle: .alert)
        
        // Create OK button with action handler
        let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
        })
    
        
        //Add OK and Cancel button to dialog message
        dialogMessage.addAction(ok)
        
        // Present dialog message to user
        self.present(dialogMessage, animated: true, completion: nil)
    }
    func displayAlcoolAlert() {
        // Declare Alert message
        let dialogMessage = UIAlertController(title: "Information Alcool", message: "Renseigner le niveau de beuverie que vous souhaitez avoir par défaut 🍺\n\nCette information n'est pas visible des autres joueurs.", preferredStyle: .alert)
        
        // Create OK button with action handler
        let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
        })
    
        
        //Add OK and Cancel button to dialog message
        dialogMessage.addAction(ok)
        
        // Present dialog message to user
        self.present(dialogMessage, animated: true, completion: nil)
    }
    
    
    
    
    //MARK: - ViewDidoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // set date picker timezone
        DatePicker.timeZone = TimeZone.init(identifier: "UTC")
        // segmented controler appearance
        UISegmentedControl.appearance().selectedSegmentTintColor = orange
        UISegmentedControl.appearance().setTitleTextAttributes([.foregroundColor: UIColor.white], for: .selected)
        UISegmentedControl.appearance().setTitleTextAttributes([.foregroundColor: UIColor.white], for: .normal)
    }
}

    //MARK: - Extention (Delegate)




