
//  UDPClientManager.swift
//  Yule
//
//  Created by PAJAK Alexandre on 28/07/2022.
//
//  * Connect, disconnect, send and receive functions for IOS -> server TCP connection *

import Foundation
import Network

class UdPlistener: Thread {
    override func main() { // Thread's starting point
        print("Hi from thread")
    }
}

class UDPClient {
    let connection: UDPClientConnection
    let host: NWEndpoint.Host
    let port: NWEndpoint.Port
    static var serverResponse = "No UDP reponse"
    
    init(host: String, port: UInt16) {
        self.host = NWEndpoint.Host(host)
        self.port = NWEndpoint.Port(rawValue: port)!
        let nwConnection = NWConnection(host: self.host, port: self.port, using: .udp)
        connection = UDPClientConnection(nwConnection: nwConnection)
    }
    
    func start() {
        print("UDP Client started \(host) \(port)")
        connection.didStopCallback = didStopCallback(error:)
        connection.start()
        print("UDP started")
    }
    
    func stop() {
        connection.stop()
    }
    func newConnectionRequest(dumpUser:String)-> String{
        print("UDP New Connection request")
        var response = UDPClient.serverResponse
        let data = "*NEW_LOBBY_CONNECTION*\(dumpUser)"
        send(data: data.data(using: .utf8)!)
        var i = 0
        while response == "No UDP reponse" || i < 100 {
            i += 1
            response = UDPClient.serverResponse
            usleep(500)
        }
        return response
    }
    
//    func getResponse(data:Data)-> String{
//        print("!! GET RESPONSE !!")
//        print("base:\(UDPClient.serverResponse)")
//        UDPClient.serverResponse = "No UDP reponse"
//        var response = UDPClient.serverResponse
//        print("reset:\(response)")
//        sleep(1)
//        send(data: data)
//        while response == "No UDP reponse"{
//            response = UDPClient.serverResponse
//            usleep(100000)
//        }
//        print("return:\(response)")
//        print("--------------------------------------")
//        return response
//    }
    // BUG!!!!! need two send for get response !!!! BUG //
    func send(data: Data){
        connection.send(data: data)
    }
    
    func didStopCallback(error: Error?) {
        if error == nil {
            exit(EXIT_SUCCESS)
        } else {
            exit(EXIT_FAILURE)
        }
    }
}

class UDPClientConnection {
    
    let  nwConnection: NWConnection
    
    init(nwConnection: NWConnection) {
        self.nwConnection = nwConnection
    }
    
    var didStopCallback: ((Error?) -> Void)? = nil
    
    func start() {
        print("UDP connection will start")
        nwConnection.stateUpdateHandler = stateDidChange(to:)
        print(setupReceive())
        nwConnection.start(queue: .global())
    }
    
    private func stateDidChange(to state: NWConnection.State) {
        switch state {
        case .waiting(let error):
            connectionDidFail(error: error)
        case .ready:
            print("UDP Client connection ready")
        case .failed(let error):
            connectionDidFail(error: error)
        default:
            break
        }
    }
    private func setupReceive(){
        nwConnection.receive(minimumIncompleteLength: 1, maximumLength: 65536) { (data, _, isComplete, error) in
            if let data = data, !data.isEmpty {
                let message = String(data: data, encoding: .utf8)
                print("UDP connection did receive, data: \(data as NSData) string: \(message ?? "-" )")
                UDPClient.serverResponse = message ?? "ND"
            }
            if isComplete {
                self.setupReceive()
                //self.connectionDidEnd()
            } else if let error = error {
                self.connectionDidFail(error: error)
            } else {
               self.setupReceive()
            }
        }
    }

        
    func send(data: Data){
        nwConnection.send(content: data, completion: .contentProcessed( { error in
            if let error = error {
                self.connectionDidFail(error: error)
                return
            }
                print("UDP connection did send, data: \(data as NSData)")
                print(data)
                self.setupReceive()
            
        }))
    }

    
    func stop() {
        print("UDP connection will stop")
        stop(error: nil)
    }
    
    private func connectionDidFail(error: Error) {
        print("UDP connection did fail, error: \(error)")
        self.stop(error: error)
    }
    
    private func connectionDidEnd() {
        print("UDP connection did end")
        self.stop(error: nil)
    }
    
    private func stop(error: Error?) {
        self.nwConnection.stateUpdateHandler = nil
        self.nwConnection.cancel()
        if let didStopCallback = self.didStopCallback {
            self.didStopCallback = nil
            didStopCallback(error)
        }
    }
}
