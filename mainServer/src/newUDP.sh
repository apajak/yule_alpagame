# UDP_barbu docker generator script
#!/bin/bash
# paja444

# Script Vars:
DIR_NAME="Lobby-$1"
UDP_Host="$2";
UDP_Port=$3;
UDP_Name="$4";
DIR_LOBBIES="/home/paja444/yule_alpagame/mainServer/Lobbies/";
DIR_PATH="${DIR_LOBBIES}${DIR_NAME}";

# insert for .env
insert_Host="HOST_IP='0.0.0.0'"
insert_Port="HOST_PORT=${UDP_Port}"
insert_Name="UDP_NAME='${UDP_Name}'"
insert_Dir="DIR_PATH='${DIR_PATH}'"

# Preflight checks:
echo "open port"
sudo ufw allow $UDP_Port/udp
echo "create dir"

# Main function:
main() {
    # Create directory/files
    echo "Creating new UDP container"
    echo "start Lobby creation..";
    mkdir -p ${DIR_PATH};
    echo "Directory creation OK.";
    echo "Create docker-compose.yml";
    touch ${DIR_PATH}/docker-compose.yml;
    echo "docker-compose.yml creation OK.";
    echo "Create .env";
    touch ${DIR_PATH}/.env;
    echo ".env creation OK.";
    # insert into .env
    echo "Insert into .env";
    echo ${insert_Host} >> ${DIR_PATH}/.env;
    echo ${insert_Port} >> ${DIR_PATH}/.env;
    echo ${insert_Name} >> ${DIR_PATH}/.env;
    echo ${insert_Dir} >> ${DIR_PATH}/.env;
    echo "Insert into .env OK.";
    # insert into docker-compose.yml
    echo "Insert into docker-compose.yml";
    echo "version: '3'" >> ${DIR_PATH}/docker-compose.yml;
    echo "services:" >> ${DIR_PATH}/docker-compose.yml;
    echo "  udp:" >> ${DIR_PATH}/docker-compose.yml;
    echo "    image: barbu:latest" >> ${DIR_PATH}/docker-compose.yml;
    echo "    container_name: ${UDP_Name}" >> ${DIR_PATH}/docker-compose.yml;
    echo "    ports:" >> ${DIR_PATH}/docker-compose.yml;
    echo "      - '${UDP_Port}:${UDP_Port}/udp'" >> ${DIR_PATH}/docker-compose.yml;
    echo "    env_file: .env" >> ${DIR_PATH}/docker-compose.yml;
    echo "    build:" >> ${DIR_PATH}/docker-compose.yml;
    echo "      context: /home/paja444/yule_alpagame/mainServer/Dockerfile" >> ${DIR_PATH}/docker-compose.yml;
    echo "      dockerfile: Dockerfile" >> ${DIR_PATH}/docker-compose.yml;
    echo "Insert into docker-compose.yml OK.";
    # start docker
    echo "start docker";
    cd ${DIR_PATH};
    #docker compose up -d;
    docker compose up;
    echo "start docker OK.";
    echo "UDP container created.";
    }
main

