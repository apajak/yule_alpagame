from dataclasses import dataclass
from datetime import datetime, date
from locale import strcoll
from typing import Any, List, TypeVar, Type, cast, Callable
import dateutil.parser
from playerManager import Player
from userManager import User, jsonParse_User
import json
import uuid

T = TypeVar("T")


def from_str(x: Any) -> str:
    assert isinstance(x, str)
    return x


def from_datetime(x: Any) -> datetime:
    return dateutil.parser.parse(x)


def from_int(x: Any) -> int:
    assert isinstance(x, int) and not isinstance(x, bool)
    return x


def from_bool(x: Any) -> bool:
    assert isinstance(x, bool)
    return x


def to_class(c: Type[T], x: Any) -> dict:
    assert isinstance(x, c)
    return cast(Any, x).to_dict()


def from_list(f: Callable[[Any], T], x: Any) -> List[T]:
    assert isinstance(x, list)
    return [f(y) for y in x]


@dataclass
class Barbu:
    # game properties
    players: List[Player]
    cards: List[List[str]]
    current_card: str
    playerIndex: int
    uuid: str
    gameMaster_uuid: str

    # game user data properties
    sugestedSipersList: List[Player]
    soberList: List[Player]
    lowDrunkList: List[Player]
    midleDrunkList: List[Player]
    highDrunkList: List[Player]
    coupleList: List[Player]
    singleList: List[Player]
    maleList: List[Player]
    femaleList: List[Player]

    whores: List[Player]
    pimpPlayer: List[Player]
    kingOfIcePlayer: List[Player]

    # cards properties
    shiFuMi: bool
    iNever: bool
    withoutSC: bool
    iHave: bool
    inMyCase: bool
    BtB: bool
    Valkyries: bool
    pimp: bool
    kingOfIce: bool
    # card value
    give: bool
    take: bool
    value: int

    def newUuid(self):
        self.uuid = str(uuid.uuid4())

    @staticmethod
    def from_dict(obj: Any) -> "Barbu":
        assert isinstance(obj, dict)

        players = from_list(Player.from_dict, obj.get("players"))
        cards = from_list(lambda x: from_list(from_str, x), obj.get("cards"))
        current_card = from_str(obj.get("current_card"))
        playerIndex = from_int(obj.get("playerIndex"))
        uuid = from_str(obj.get("uuid"))
        gameMaster_uuid = from_str(obj.get("gameMaster_uuid"))

        sugestedSipersList = from_list(Player.from_dict, obj.get("sugestedSipersList"))
        soberList = from_list(Player.from_dict, obj.get("soberList"))
        lowDrunkList = from_list(Player.from_dict, obj.get("lowDrunkList"))
        midleDrunkList = from_list(Player.from_dict, obj.get("midleDrunkList"))
        highDrunkList = from_list(Player.from_dict, obj.get("highDrunkList"))
        coupleList = from_list(Player.from_dict, obj.get("coupleList"))
        singleList = from_list(Player.from_dict, obj.get("singleList"))
        maleList = from_list(Player.from_dict, obj.get("maleList"))
        femaleList = from_list(Player.from_dict, obj.get("femaleList"))

        whores = from_list(Player.from_dict, obj.get("whores"))
        pimpPlayer = from_list(Player.from_dict, obj.get("pimpPlayer"))
        kingOfIcePlayer = from_list(Player.from_dict, obj.get("kingOfIce"))

        shiFuMi = from_bool(obj.get("shiFuMi"))
        iNever = from_bool(obj.get("iNever"))
        withoutSC = from_bool(obj.get("withoutSC"))
        iHave = from_bool(obj.get("iHave"))
        inMyCase = from_bool(obj.get("inMyCase"))
        BtB = from_bool(obj.get("BtB"))
        Valkyries = from_bool(obj.get("Valkyries"))
        pimp = from_bool(obj.get("pimp"))
        kingOfIce = from_bool(obj.get("kingOfIce"))

        give = from_bool(obj.get("give"))
        take = from_bool(obj.get("take"))
        value = from_int(obj.get("value"))

        return Barbu(
            players,
            cards,
            current_card,
            playerIndex,
            uuid,
            gameMaster_uuid,
            sugestedSipersList,
            soberList,
            lowDrunkList,
            midleDrunkList,
            highDrunkList,
            coupleList,
            singleList,
            maleList,
            femaleList,
            whores,
            pimpPlayer,
            kingOfIcePlayer,
            shiFuMi,
            iNever,
            withoutSC,
            iHave,
            inMyCase,
            BtB,
            Valkyries,
            pimp,
            kingOfIce,
            give,
            take,
            value,
        )

    def to_dict(self) -> dict:
        result: dict = {}
        if len(self.players) > 0:
            result["players"] = from_list(lambda x: to_class(Player, x), self.players)
        else:
            result["players"] = []

        if len(self.cards) > 0:
            result["cards"] = from_list(lambda x: from_list(from_str, x), self.cards)
        else:
            result["cards"] = []

        result["current_card"] = from_str(self.current_card)
        result["playerIndex"] = from_int(self.playerIndex)
        result["uuid"] = from_str(self.uuid)
        result["gameMaster_uuid"] = from_str(self.gameMaster_uuid)

        if len(self.sugestedSipersList) > 0:
            result["sugestedSipersList"] = from_list(lambda x: to_class(Player, x), self.sugestedSipersList)
        else:
            result["sugestedSipersList"] = []

        if len(self.soberList) > 0:
            result["soberList"] = from_list(
                lambda x: to_class(Player, x), self.soberList
            )
        else:
            result["soberList"] = []

        if len(self.lowDrunkList) > 0:
            result["lowDrunkList"] = from_list(
                lambda x: to_class(Player, x), self.lowDrunkList
            )
        else:
            result["lowDrunkList"] = []

        if len(self.midleDrunkList) > 0:
            result["midleDrunkList"] = from_list(
                lambda x: to_class(Player, x), self.midleDrunkList
            )
        else:
            result["midleDrunkList"] = []

        if len(self.highDrunkList) > 0:
            result["highDrunkList"] = from_list(
                lambda x: to_class(Player, x), self.highDrunkList
            )
        else:
            result["highDrunkList"] = []

        if len(self.coupleList) > 0:
            result["coupleList"] = from_list(
                lambda x: to_class(Player, x), self.coupleList
            )
        else:
            result["coupleList"] = []

        if len(self.singleList) > 0:
            result["singleList"] = from_list(
                lambda x: to_class(Player, x), self.singleList
            )
        else:
            result["singleList"] = []

        if len(self.maleList) > 0:
            result["maleList"] = from_list(lambda x: to_class(Player, x), self.maleList)
        else:
            result["maleList"] = []

        if len(self.femaleList) > 0:
            result["femaleList"] = from_list(
                lambda x: to_class(Player, x), self.femaleList
            )
        else:
            result["femaleList"] = []

        if len(self.whores) > 0:
            result["whores"] = from_list(lambda x: to_class(Player, x), self.whores)
        else:
            result["whores"] = []

        if len(self.pimpPlayer) > 0:
            result["pimpPlayer"] = from_list(
                lambda x: to_class(Player, x), self.pimpPlayer
            )
        else:
            result["pimpPlayer"] = []

        if len(self.kingOfIcePlayer) > 0:
            result["kingOfIcePlayer"] = from_list(
                lambda x: to_class(Player, x), self.kingOfIcePlayer
            )
        else:
            result["kingOfIcePlayer"] = []

        result["shiFuMi"] = from_bool(self.shiFuMi)
        result["iNever"] = from_bool(self.iNever)
        result["withoutSC"] = from_bool(self.withoutSC)
        result["iHave"] = from_bool(self.iHave)
        result["inMyCase"] = from_bool(self.inMyCase)
        result["BtB"] = from_bool(self.BtB)
        result["Valkyries"] = from_bool(self.Valkyries)
        result["pimp"] = from_bool(self.pimp)
        result["kingOfIce"] = from_bool(self.kingOfIce)
        result["give"] = from_bool(self.give)
        result["take"] = from_bool(self.take)
        result["value"] = from_int(self.value)
        return result