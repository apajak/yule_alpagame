//
//  EndGameController.swift
//  Yule
//
//  Created by PAJAK Alexandre on 29/12/2022.
//

import Foundation
import UIKit

class ScoreTableViewCell:UITableViewCell{
    
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var scoreView: UIView!{
        didSet{
            // Passwd Field design
            let cornerRAdius = 10
            scoreView.layer.cornerRadius = CGFloat(cornerRAdius)
            scoreView.layer.borderColor = UIColor.white.cgColor
            scoreView.layer.borderWidth = 2
            scoreView.layer.masksToBounds = (cornerRAdius != 0)
        }
    }
}
class EndGameController: UIViewController {
    
    //MARK: - Gobal var
    //MARK: - outlet
    
    @IBOutlet weak var ScoreTableView: UITableView!
    
    
    @IBOutlet weak var backHomeButton: UIButton!{
        didSet{
            // Passwd Field design
            let cornerRAdius = 10
            backHomeButton.layer.cornerRadius = CGFloat(cornerRAdius)
            backHomeButton.layer.borderColor = UIColor.white.cgColor
            backHomeButton.layer.borderWidth = 2
            backHomeButton.layer.masksToBounds = (cornerRAdius != 0)
        }
    }
    
    //MARK: - Action
    
    @IBAction func backHomeButtonDidTouch(_ sender: Any) {
        
        
    }
    
    //MARK: - Functions
    //MARK: - ViewDidoad"
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
//MARK: - Extention (Delegate)
extension EndGameController: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return BarbuController.game.players.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "scoreCell", for: indexPath) as! ScoreTableViewCell
        let user = BarbuController.game.players[indexPath.row].user
        print(user.userName)
        cell.nameLabel.text = user.userName
        cell.scoreLabel.text = String(user.totalSips)
        return cell
    }
}
