//
//  GameManager.swift
//  Yule
//
//  Created by PAJAK Alexandre on 09/08/2022.
//

import Foundation

// MARK: - Barbu
class Barbu: Codable {
    var players: [Player]
    var cards: [[String]]
    var current_card : String
    var playerIndex: Int
    var uuid: String
    var gameMaster_uuid: String
    
    var sugestedSipersList: [Player]
    var soberList: [Player]
    var lowDrunkList: [Player]
    var midleDrunkList: [Player]
    var highDrunkList: [Player]
    var coupleList: [Player]
    var singleList: [Player]
    var maleList: [Player]
    var femaleList: [Player]

    var whores: [Player]
    var pimpPlayer: [Player]
    var kingOfIcePlayer: [Player]
    
    var shiFuMi: Bool
    var iNever: Bool
    var withoutSC: Bool
    var iHave: Bool
    var inMyCase: Bool
    var BtB: Bool
    var Valkyries: Bool
    var pimp: Bool
    var kingOfIce: Bool
    var give: Bool
    var take: Bool
    var value: Int
    
    enum CodingKeys: String, CodingKey {
        case players = "players"
        case cards = "cards"
        case current_card = "current_card"
        case playerIndex = "playerIndex"
        case uuid = "uuid"
        case gameMaster_uuid = "gameMaster_uuid"
        
        case sugestedSipersList = "sugestedSipersList"
        case soberList = "soberList"
        case lowDrunkList = "lowDrunkList"
        case midleDrunkList = "midleDrunkList"
        case highDrunkList = "highDrunkList"
        case coupleList = "coupleList"
        case singleList = "singleList"
        case maleList = "maleList"
        case femaleList = "femaleList"
        
        case whores = "whores"
        case pimpPlayer = "pimpPlayer"
        case kingOfIcePlayer = "kingOfIcePlayer"
        
        case shiFuMi = "shiFuMi"
        case iNever = "iNever"
        case withoutSC = "withoutSC"
        case iHave = "iHave"
        case inMyCase = "inMyCase"
        case BtB = "BtB"
        case Valkyries = "Valkyries"
        case pimp = "pimp"
        case kingOfIce = "kingOfIce"
        
        case give = "give"
        case take = "take"
        case value = "value"
        
    }

    init() {
        self.players = []
        self.cards = [[]]
        self.current_card = "empty"
        self.playerIndex = 0
        self.uuid = "uuid"
        self.gameMaster_uuid = "gameMaster_uuid"
        
        self.sugestedSipersList = []
        self.soberList = []
        self.lowDrunkList = []
        self.midleDrunkList = []
        self.highDrunkList = []
        self.coupleList = []
        self.singleList = []
        self.maleList = []
        self.femaleList = []
        
        
        self.whores = []
        self.pimpPlayer = []
        self.kingOfIcePlayer = []
        
        self.shiFuMi = false
        self.iNever = false
        self.withoutSC = false
        self.iHave = false
        self.inMyCase = false
        self.BtB = false
        self.Valkyries = false
        self.pimp = false
        self.kingOfIce = false
        
        self.give = false
        self.take = false
        self.value = 0
    }
}

func parseJSONBarbu(jsonString: String)-> Barbu?{
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
    let data = jsonString.data(using: .utf8)!
    let decoder = JSONDecoder()
    decoder.dateDecodingStrategy = .formatted(formatter)
    let game = try! decoder.decode(Barbu.self,from:data)
    return game
}

func dumpJSONBarbu(barbu:Barbu)->String{
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
    let jsonifyBarbu : String
    let encoder = JSONEncoder()
    encoder.dateEncodingStrategy = .formatted(formatter)
    let jsonData = try! encoder.encode(barbu)
    jsonifyBarbu = String(data: jsonData, encoding: .utf8)!
    return jsonifyBarbu
}

