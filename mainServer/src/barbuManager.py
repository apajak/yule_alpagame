## YULE PROJECT ##
## Barbu Class and functions for UDP_barbu.py ##
# Alexandre Pajak 18/05/2022


from dataclasses import dataclass
from datetime import datetime, date
from locale import strcoll
from typing import Any, List, TypeVar, Type, cast, Callable
import dateutil.parser
from playerManager import Player
from userManager import User, jsonParse_User
import json
import uuid

T = TypeVar("T")


def from_str(x: Any) -> str:
    assert isinstance(x, str)
    return x


def from_datetime(x: Any) -> datetime:
    return dateutil.parser.parse(x)


def from_int(x: Any) -> int:
    assert isinstance(x, int) and not isinstance(x, bool)
    return x


def from_bool(x: Any) -> bool:
    assert isinstance(x, bool)
    return x


def to_class(c: Type[T], x: Any) -> dict:
    assert isinstance(x, c)
    return cast(Any, x).to_dict()


def from_list(f: Callable[[Any], T], x: Any) -> List[T]:
    assert isinstance(x, list)
    return [f(y) for y in x]

@dataclass
class Barbu:
    # game properties
    players: List[Player]
    cards: List[List[str]]
    current_card: str
    playerIndex: int
    uuid: str
    gameMaster_uuid: str

    # game user data properties
    sugestedSipersList: List[Player]
    soberList: List[Player]
    lowDrunkList: List[Player]
    midleDrunkList: List[Player]
    highDrunkList: List[Player]
    coupleList: List[Player]
    singleList: List[Player]
    maleList: List[Player]
    femaleList: List[Player]

    whores: List[Player]
    pimpPlayer: List[Player]
    kingOfIcePlayer: List[Player]

    # cards properties
    shiFuMi: bool
    iNever: bool
    withoutSC: bool
    iHave: bool
    inMyCase: bool
    BtB: bool
    Valkyries: bool
    pimp: bool
    kingOfIce: bool
    # card value
    give: bool
    take: bool
    value: int

    def newUuid(self):
        self.uuid = str(uuid.uuid4())

    @staticmethod
    def from_dict(obj: Any) -> "Barbu":
        assert isinstance(obj, dict)

        players = from_list(Player.from_dict, obj.get("players"))
        cards = from_list(lambda x: from_list(from_str, x), obj.get("cards"))
        current_card = from_str(obj.get("current_card"))
        playerIndex = from_int(obj.get("playerIndex"))
        uuid = from_str(obj.get("uuid"))
        gameMaster_uuid = from_str(obj.get("gameMaster_uuid"))
        sugestedSipersList = from_list(Player.from_dict, obj.get("sugestedSipersList"))
        soberList = from_list(Player.from_dict, obj.get("soberList"))
        lowDrunkList = from_list(Player.from_dict, obj.get("lowDrunkList"))
        midleDrunkList = from_list(Player.from_dict, obj.get("midleDrunkList"))
        highDrunkList = from_list(Player.from_dict, obj.get("highDrunkList"))
        coupleList = from_list(Player.from_dict, obj.get("coupleList"))
        singleList = from_list(Player.from_dict, obj.get("singleList"))
        maleList = from_list(Player.from_dict, obj.get("maleList"))
        femaleList = from_list(Player.from_dict, obj.get("femaleList"))

        whores = from_list(Player.from_dict, obj.get("whores"))
        pimpPlayer = from_list(Player.from_dict, obj.get("pimpPlayer"))
        kingOfIcePlayer = from_list(Player.from_dict, obj.get("kingOfIcePlayer"))

        shiFuMi = from_bool(obj.get("shiFuMi"))
        iNever = from_bool(obj.get("iNever"))
        withoutSC = from_bool(obj.get("withoutSC"))
        iHave = from_bool(obj.get("iHave"))
        inMyCase = from_bool(obj.get("inMyCase"))
        BtB = from_bool(obj.get("BtB"))
        Valkyries = from_bool(obj.get("Valkyries"))
        pimp = from_bool(obj.get("pimp"))
        kingOfIce = from_bool(obj.get("kingOfIce"))

        give = from_bool(obj.get("give"))
        take = from_bool(obj.get("take"))
        value = from_int(obj.get("value"))

        return Barbu(
            players,
            cards,
            current_card,
            playerIndex,
            uuid,
            gameMaster_uuid,
            sugestedSipersList,
            soberList,
            lowDrunkList,
            midleDrunkList,
            highDrunkList,
            coupleList,
            singleList,
            maleList,
            femaleList,
            whores,
            pimpPlayer,
            kingOfIcePlayer,
            shiFuMi,
            iNever,
            withoutSC,
            iHave,
            inMyCase,
            BtB,
            Valkyries,
            pimp,
            kingOfIce,
            give,
            take,
            value,
        )

    def to_dict(self) -> dict:
        result: dict = {}
        if len(self.players) > 0:
            result["players"] = from_list(lambda x: to_class(Player, x), self.players)
        else:
            result["players"] = []

        if len(self.cards) > 0:
            result["cards"] = from_list(lambda x: from_list(from_str, x), self.cards)
        else:
            result["cards"] = []

        result["current_card"] = from_str(self.current_card)
        result["playerIndex"] = from_int(self.playerIndex)
        result["uuid"] = from_str(self.uuid)
        result["gameMaster_uuid"] = from_str(self.gameMaster_uuid)

        if len(self.sugestedSipersList) > 0:
            result["sugestedSipersList"] = from_list(lambda x: to_class(Player, x), self.sugestedSipersList)
        else:
            result["sugestedSipersList"] = []

        if len(self.soberList) > 0:
            result["soberList"] = from_list(
                lambda x: to_class(Player, x), self.soberList
            )
        else:
            result["soberList"] = []

        if len(self.lowDrunkList) > 0:
            result["lowDrunkList"] = from_list(
                lambda x: to_class(Player, x), self.lowDrunkList
            )
        else:
            result["lowDrunkList"] = []

        if len(self.midleDrunkList) > 0:
            result["midleDrunkList"] = from_list(
                lambda x: to_class(Player, x), self.midleDrunkList
            )
        else:
            result["midleDrunkList"] = []

        if len(self.highDrunkList) > 0:
            result["highDrunkList"] = from_list(
                lambda x: to_class(Player, x), self.highDrunkList
            )
        else:
            result["highDrunkList"] = []

        if len(self.coupleList) > 0:
            result["coupleList"] = from_list(
                lambda x: to_class(Player, x), self.coupleList
            )
        else:
            result["coupleList"] = []

        if len(self.singleList) > 0:
            result["singleList"] = from_list(
                lambda x: to_class(Player, x), self.singleList
            )
        else:
            result["singleList"] = []

        if len(self.maleList) > 0:
            result["maleList"] = from_list(lambda x: to_class(Player, x), self.maleList)
        else:
            result["maleList"] = []

        if len(self.femaleList) > 0:
            result["femaleList"] = from_list(
                lambda x: to_class(Player, x), self.femaleList
            )
        else:
            result["femaleList"] = []

        if len(self.whores) > 0:
            result["whores"] = from_list(lambda x: to_class(Player, x), self.whores)
        else:
            result["whores"] = []

        if len(self.pimpPlayer) > 0:
            result["pimpPlayer"] = from_list(
                lambda x: to_class(Player, x), self.pimpPlayer
            )
        else:
            result["pimpPlayer"] = []

        if len(self.kingOfIcePlayer) > 0:
            result["kingOfIcePlayer"] = from_list(
                lambda x: to_class(Player, x), self.kingOfIcePlayer
            )
        else:
            result["kingOfIcePlayer"] = []

        result["shiFuMi"] = from_bool(self.shiFuMi)
        result["iNever"] = from_bool(self.iNever)
        result["withoutSC"] = from_bool(self.withoutSC)
        result["iHave"] = from_bool(self.iHave)
        result["inMyCase"] = from_bool(self.inMyCase)
        result["BtB"] = from_bool(self.BtB)
        result["Valkyries"] = from_bool(self.Valkyries)
        result["pimp"] = from_bool(self.pimp)
        result["kingOfIce"] = from_bool(self.kingOfIce)
        result["give"] = from_bool(self.give)
        result["take"] = from_bool(self.take)
        result["value"] = from_int(self.value)
        return result

    # method for increment playerIndex
    def next_Player(self):
        numberOfPlayers = len(self.players)
        if self.playerIndex + 1 < numberOfPlayers:
            self.playerIndex += 1
        elif self.playerIndex + 1 == numberOfPlayers:
            self.playerIndex = 0

    def setSipers(self):
        for player in self.players:
            if player.user.alcohol == "NO":
                self.soberList.append(player)
            elif player.user.alcohol == "LOW":
                self.lowDrunkList.append(player)
            elif player.user.alcohol == "MIDLE":
                self.midleDrunkList.append(player)
            elif player.user.alcohol == "HIGH":
                self.highDrunkList.append(player)

    def resetCardsBool(self):
        try :
            self.shiFuMi = False
            self.iNever = False
            self.withoutSC = False
            self.iHave = False
            self.inMyCase = False
            self.BtB = False
            self.Valkyries = False
            self.pimp = False
            self.kingOfIce = False
            self.give = False
            self.take = False
            self.value = 0
        except Exception as e:
            print(e)


    def suggestSipers(self, current_sips: int):
        # feed the lists
        suggestedSipers = []
        current_soberList = []
        current_lowDrunkList = []
        current_midleDrunkList = []
        current_highDrunkList = []
        sober_tolerance = [0, 0]
        lowDrunk_tolerance = [1, 10]
        midleDrunk_tolerance = [11, 20]
        highDrunk_tolerance = [21, 100]

        self.setSipers()

        for player in self.players:
            if player in self.soberList:
                current_soberList.append(player)
            else:
                if (
                    sober_tolerance[0]
                    <= (player.currentSips + current_sips)
                    <= sober_tolerance[1]
                ):
                    current_soberList.append(player)
                elif (
                    lowDrunk_tolerance[0]
                    <= (player.currentSips + current_sips)
                    <= lowDrunk_tolerance[1]
                ):
                    current_lowDrunkList.append(player)
                elif (
                    midleDrunk_tolerance[0]
                    <= (player.currentSips + current_sips)
                    <= midleDrunk_tolerance[1]
                ):
                    current_midleDrunkList.append(player)
                elif (
                    highDrunk_tolerance[0]
                    <= (player.currentSips + current_sips)
                    <= highDrunk_tolerance[1]
                ):
                    current_highDrunkList.append(player)
                # ---------------------------------------
                # final list
                # ---------------------------------------
                for player in current_soberList:
                    if player not in self.soberList:
                        suggestedSipers.append(player)
                for player in current_lowDrunkList:
                    if player in self.lowDrunkList:
                        suggestedSipers.append(player)
                for player in current_midleDrunkList:
                    if player in self.midleDrunkList:
                        suggestedSipers.append(player)
                for player in current_highDrunkList:
                    if player in self.highDrunkList:
                        suggestedSipers.append(player)

                self.soberList = current_soberList
                self.lowDrunkList = current_lowDrunkList
                self.midleDrunkList = current_midleDrunkList
                self.highDrunkList = current_highDrunkList
                self.sugestedSipersList = suggestedSipers

    def processCard(self):
        print("Processing card: " + self.current_card + " ...")
        value = int(self.current_card[0:2])
        color = self.current_card[2]
        current_player = self.players[self.playerIndex]
        self.sugestedSipersList = []
        self.resetCardsBool()
        # For 2,3,4 cards: black give & red drink:
        if 2 <= value <= 4:
            if color == "C" or color == "S":
                self.give = True
                self.value = value
            elif color == "D" or color == "H":
                self.take = True
                self.value = value
                self.suggestSipers(value)

        # For 5 cards whore rule:
        if value == 5:
            self.pimp = True
            self.pimpPlayer.clear()
            self.pimpPlayer.append(current_player)
            self.sugestedSipersList = self.midleDrunkList + self.highDrunkList

        # For 6 cards shi fu mi rule:
        if value == 6:
            self.shiFuMi = True
            self.sugestedSipersList = self.midleDrunkList + self.highDrunkList
        # For 7 cards never do rule:
        if value == 7:
            self.iNever = True
        if value == 8:
            self.withoutSC = True
        if value == 9:
            self.iHave = True
        if value == 10:
            self.inMyCase = True
        if value == 11:
            self.BtB = True
        if value == 12:
            self.Valkyries = True
            self.sugestedSipersList = self.midleDrunkList + self.highDrunkList
        if value == 13:
            self.kingOfIce = True
            self.kingOfIcePlayer.clear()
            self.kingOfIcePlayer.append(current_player)
        if value == 14:
            if color == "C" or color == "S":
                self.give = True
                self.value = 10
                self.suggestSipers(10)
            elif color == "D" or color == "H":
                self.take = True
                self.value = 10
                self.suggestSipers(10)
        print(f"[Final]json_String: {jsonDumps_Barbu(self)}")




################## JSON #####################
# Convert class.attribut to dict
def Barbufromdict(s: Any) -> Barbu:
    return Barbu.from_dict(s)


# Parse Json for return Barbu Object
def jsonParse_Barbu(json_string: str) -> Barbu:
    result = Barbufromdict(json.loads(json_string))
    return result


# Resolv Date Conflict (convert Datetime Object to iso8601 string)
def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""
    if isinstance(obj, (datetime, date)):
        return obj.isoformat()
    raise TypeError("Type %s not serializable" % type(obj))


# Dumps Barbu oject to Json string
def jsonDumps_Barbu(game: Barbu) -> str:
    barbuDict = Barbu.to_dict(game)
    return json.dumps(barbuDict, default=json_serial)


def findGame(gameList: list, uuid: str) -> Barbu:
    for game in gameList:
        if game.uuid == uuid:
            return game

#############################################

######### TEST (work on class. Not actualized test):########
# strJson = '''{
# "players": [
#     {
#         "user": {"uuid": "7a869e59-22d5-4af8-a632-570b3eb015b5", "userName": "Alex", "firstName": "alexandre", "lastName": "Pajak", "password": "$2a$10$vj9CAq03e94O9CjqP45IxOpluWw8MyEX0AwqrvHOIKWd2/KRQg5B2", "email": "jakpa.sas@gmail.com", "birthDate": "1998-11-10T00:00:00", "gender": "Homme", "sexualOrientation": "Femme", "coupleState": "Celibataire", "alcohol": "Beaucoup", "totalSips": 0, "isConnect": false, "currentTCPAddress": "127.0.0.1:52735", "currentUDPAddress": "127.0.0.1:52735"},
#         "currentSips": 0,
#         "isConnect": false
#     },
#     {
#         "user": {"uuid": "7a869e59-22d5-4af8-a632-570b3eb015b5", "userName": "Alex", "firstName": "alexandre", "lastName": "Pajak", "password": "$2a$10$vj9CAq03e94O9CjqP45IxOpluWw8MyEX0AwqrvHOIKWd2/KRQg5B2", "email": "jakpa.sas@gmail.com", "birthDate": "1998-11-10T00:00:00", "gender": "Homme", "sexualOrientation": "Femme", "coupleState": "Celibataire", "alcohol": "Beaucoup", "totalSips": 0, "isConnect": false, "currentTCPAddress": "127.0.0.1:52735", "currentUDPAddress": "127.0.0.1:52735"},
#         "currentSips": 0,
#         "isConnect": false
#     }
# ],
# "cards": [
#     [
#         "05D",
#         "12H",
#         "02H",
#         "09D",
#         "14C",
#         "09C",
#         "08S",
#         "11S",
#         "07C",
#         "03C",
#         "05S",
#         "03H",
#         "12S",
#         "13S",
#         "02D",
#         "07D",
#         "12D",
#         "04D",
#         "02S",
#         "12C",
#         "03S",
#         "10C",
#         "04C",
#         "02J",
#         "03D",
#         "13H",
#         "10D",
#         "11H",
#         "06D",
#         "10S",
#         "06S",
#         "05H",
#         "07S",
#         "08D",
#         "09H",
#         "07H",
#         "01J",
#         "13D",
#         "14H",
#         "14D",
#         "04S",
#         "02C",
#         "11C",
#         "13C",
#         "08H",
#         "04H",
#         "14S",
#         "08C",
#         "10H",
#         "09S",
#         "06C",
#         "05C",
#         "11D",
#         "06H"
#     ]
# ],
# "playerIndex": 1
# }'''
# strJson = '''*CLIENTUPDATEGAME*{"players":[{"isConnect":true,"user":{"uuid":"b5c18a5f-4251-4c74-a908-9ce6563ebd0f","coupleState":"Celibataire","firstName":"Pierre-Alain","birthDate":"1998-11-17T14:13:23+0100","lastName":"Lorrain","isConnect":true,"userName":"PA","password":"$2a$10$vm4xQq61RBaLzGLPGlS\/LOtRvSuEcCzWLahhFpJ04o6idk98vy\/FO","sexualOrientation":"Femme","alcohol":"Beaucoup","currentUDPAddress":"127.0.0.1:60185","totalSips":0,"currentTCPAddress":"","email":"pa@gmail.com","gender":"Homme"},"currentSips":0}],"soberList":[],"whores":[],"iNever":false,"inMyCase":false,"BtB":false,"iHave":false,"pimp":false,"midleDrunkList":[],"femaleList":[],"uuid":"1c7410f3-548f-430d-9e9b-202dd2c5e34f","sugestedSipersList":[],"withoutSC":false,"maleList":[],"playerIndex":0,"coupleList":[],"lowDrunkList":[],"current_card":"000.png","singleList":[],"gameMaster_uuid":"b5c18a5f-4251-4c74-a908-9ce6563ebd0f","cards":[["13S","12D","09H","08C","14D","09D","09C","06D","10S","05D","06S","03D","08S","09S","11S","07D","03H","02D","08D","14C","04S","02S","13D","11C","04C","04H","10C","06H","03S","11D","02C","12C","12S","05H","14S","08H","04D","13H","06C","14H","07H","10H","03C","10D","05C","07C","11H","07S","05S","02H","12H","13C"]],"highDrunkList":[],"kingOfIce":false,"give":false,"take":false,"value":0,"Valkyries":false,"kingOfIcePlayer":[],"shiFuMi":false,"pimpPlayer":[]}'''
# message = strJson.split("*CLIENTUPDATEGAME*")[1]
# print(message)
# game = jsonParse_Barbu(message)
# print(game.cards)
# print(game.players)
# print(game.cards)
# print(game.players[0].user.firstName)
