//
//  HashManager.swift
//  Yule
//
//  Created by PAJAK Alexandre on 27/06/2022.
//

import Foundation
import BCryptSwift

func generateSalt() -> String {
    var salt : String
    salt = BCryptSwift.generateSalt()
    return salt
}

func createHash(InputPassword: String) -> String {
    if let hash = BCryptSwift.hashPassword(InputPassword, withSalt: generateSalt()) {
        return hash
    }
    else {
        print("Hash generation failed")
        return "Error"
    }
}

func compareHash(InputPassword: String, hash: String) -> Bool {
    if let compare = BCryptSwift.verifyPassword(InputPassword, matchesHash: hash) {
        if compare {
            print("Hash comparison Success!")
            return true
        }
        else {
            print("Hash comparison Not Match!")
            return false
        }
    }
    else {
        print("Hash comparison failed!")
        return false
    }
}
