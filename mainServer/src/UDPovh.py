## YULE PROJECT ##
## UDP Python server for lobby and Games ##
# Alexandre Pajak 04/08/2022

### Game Server ###
# from dotenv import load_dotenv
import os
import socket
from UDPManager import UDP, jsonDumps_UDP, UDPlist, cleanUP
from playerManager import Player
from sqliteManager import updateDBUser, getUser
from time import sleep
from userManager import (
    User,
    encode_client_address,
    jsonParse_User,
)
from cardManager import cards_create_deck
from barbuManager import Barbu, jsonDumps_Barbu, jsonParse_Barbu

# Server Global:
hostIP: str
hostPort: int
udp_Name: str
udp_DirPath: str

# load_dotenv(".env")
hostIP = "54.37.153.179"
hostPort = 20001
udp_Name = "alexDefault"
# udp_DirPath = os.getenv("DIR_PATH")
print(hostIP, hostPort, udp_Name)

bufferSize = 2048
msgFromServer = "Hello UDP Client"
bytesToSend = str.encode(msgFromServer)
# Create a datagram socket
UDPServerSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
# Bind to address and ip
UDPServerSocket.bind((hostIP, hostPort))
print("UDP server up and listening")

# Game Global
udp: UDP
game: Barbu
currentUser: User


####### INIT UDP Local ######
try:
    udp = UDP(udp_Name, "", "12345678", hostIP, hostPort, "", "", 0, [])
    udp.newUuid()
    # print(f"udp:{udp.name} was added.")
    print(UDPlist)
except Exception:
    print("default UDP fail")
#######################


# Listen for incoming datagrams
while True:
    try:
        # Global
        # -------------------------------------------------#
        # bytesAddressPair -> [0]= message | [1]= IP+Port tuple
        bytesAddressPair = UDPServerSocket.recvfrom(bufferSize)
        client_message = bytesAddressPair[0].decode("utf-8")
        client_address = bytesAddressPair[1]
        clientMsg = f"Message from Client:{client_message}"
        clientIP = f"Client IP Address:{client_address}"
        # -------------------------------------------------#
        # NEW Connection
        # -------------------------------------------------#
        # IF "*NEWCONNECTION*" flag in UDP receive
        if client_message.find("*NEW_LOBBY_CONNECTION*") == 0:
            print("*NEW_LOBBY_CONNECTION*")
            try:
                print("$$$$$$$ NEW UDP USER $$$$$$$")
                # Get the new udp user list
                message = client_message.split("*NEW_LOBBY_CONNECTION*")[1]
                try:
                    print("User succesffully added to UDP_client")
                    user = jsonParse_User(message)
                    udp.udp_client.append(user)
                    udp.client_number = len(udp.udp_client)
                    if udp.client_number == 1:
                        udp.master_uuid = user.uuid
                        udp.master_username = user.userName
                except Exception:
                    print("user add to UDP_client fail")
                try:
                    print("add UDP current ip to userData")
                    # tuple -> str convertion
                    str_client_address = f"{client_address[0]}:{client_address[1]}"
                    # add udp Address:
                    user.currentUDPAddress = str_client_address
                    print(user.currentUDPAddress)
                    # Add str_client_address to User sqlite table
                    updateDBUser(
                        "currentUDPAddress", str_client_address, "uuid", user.uuid
                    )
                    # Create current user
                except Exception:
                    print("add ip to userData fail")
                try:
                    print("get currentUser")
                    currentUser = getUser(user.uuid)
                except Exception:
                    print("get currentUser fail")
                # Send actualise UserList:
                try:
                    print("send actualised udp client list")
                    msgFromServer = str.encode(
                        "*NEW_LOBBY_CONNECTION*" + jsonDumps_UDP(udp)
                    )
                    for user in udp.udp_client:
                        print(user.currentUDPAddress)
                        UDPServerSocket.sendto(
                            msgFromServer, encode_client_address(user.currentUDPAddress)
                        )
                except Exception:
                    print("fail to send actualised udp clients")

            except Exception:
                print("error NewConnection")
                print(Exception)

        # -------------------------------------------------#
        # Go to game button pushed by master
        # -------------------------------------------------#
        if client_message.find("*GOGAME*") == 0:
            print("$$$$$$$ RUNGAME $$$$$$$")
            # init empty Barbu's game
            game = Barbu(
                [],
                [[]],
                "000.png",
                0,
                "uuid",
                "uuid",
                [],
                [],
                [],
                [],
                [],
                [],
                [],
                [],
                [],
                [],
                [],
                [],
                False,
                False,
                False,
                False,
                False,
                False,
                False,
                False,
                False,
                False,
                False,
                0,
            )
            game.cards = cards_create_deck(52, 1, False)
            game.uuid = udp.uuid
            game.gameMaster_uuid = udp.master_uuid
            # add udp client to barbu players
            for client in udp.udp_client:
                player = Player(client, 0, False)
                game.players.append(player)

            # send "*GOGAME*" flag for move automatically all lobby client in game screen
            msgFromServer = str.encode("*GOGAME*")
            for user in udp.udp_client:
                UDPServerSocket.sendto(
                    msgFromServer, encode_client_address(user.currentUDPAddress)
                )
        # -------------------------------------------------#
        # client join game screen
        # -------------------------------------------------#
        if client_message.find("*GAME_CONNECTION*") == 0:
            print("$$$$$$$ GAME_CONNECTION $$$$$$$")
            # Get the new connected user.uuid
            id = client_message.split("*GAME_CONNECTION*")[1]
            for player in game.players:
                if player.user.uuid == id:
                    player.isConnect = True
            # send actualised Barbu player list
            msgFromServer = str.encode("*GAME_CONNECTION*" + jsonDumps_Barbu(game))
            for user in udp.udp_client:
                UDPServerSocket.sendto(
                    msgFromServer, encode_client_address(user.currentUDPAddress)
                )
        # -------------------------------------------------#

        # GET Card
        # -------------------------------------------------#
        if client_message.find("*NEWCARD*") == 0:
            print("$$$$$$$ NEWCARD $$$$$$$")
            playerIndex = game.playerIndex
            currentPlayer = game.players[playerIndex]
            if len(game.cards[0]) > 0:
                card = game.cards[0].pop()
                print(f"la carte: {card}")
                # Process card
                game.current_card = card
                game.processCard()
                # Game Data update
                # game.current_description = getDescription(card=card)
                # game.next_Player()
                # send actualised Barbu
                newBarbu = jsonDumps_Barbu(game)
                msgFromServer = str.encode(f"*NEWCARD*{newBarbu}")
                for user in udp.udp_client:
                    UDPServerSocket.sendto(
                        msgFromServer, encode_client_address(user.currentUDPAddress)
                    )
            elif len(game.cards[0]) == 0:
                # close game and save résult
                pass
        # -------------------------------------------------#
        # update game
        # -------------------------------------------------#
        if client_message.find("*CLIENTUPDATEGAME*") == 0:
            print("$$$$$$$ UPDATEGAME $$$$$$$")
            print("client_message")
            message = client_message.split("*CLIENTUPDATEGAME*")[1]
            print(message)
            game = jsonParse_Barbu(message)
            game.next_Player()
            # send actualised Barbu
            msgFromServer = str.encode(f"*SERVERUPDATEGAME*{jsonDumps_Barbu(game)}")
            for user in udp.udp_client:
                UDPServerSocket.sendto(
                    msgFromServer, encode_client_address(user.currentUDPAddress)
                )
        # Disconnect (/!\bug/!\)
        # -------------------------------------------------#
        if client_message.find("*DISCONNECT*") == 0:
            print("$$$$$$$ DISCONNECT $$$$$$$")
            # for player in game.players:
            #     if player.uuid == currentUser.uuid:
            #         targetUser = findUser(game.players, currentUser.uuid)
            #         game.players.remove(player)
            #         print(f"player {player.userName}-{currentUser.uuid} is deconnected")
            #         # send reply
            #         response = str.encode(
            #             f"{currentUser.userName}-{currentUser.uuid} is disconnected."
            #         )
            #         print(targetUser.currentUDPAddress)
            #         print(msgFromServer)
            #         UDPServerSocket.sendto(
            #             response, encode_client_address(targetUser.currentUDPAddress)
            #         )
        # -------------------------------------------------#
    except Exception:
        print(Exception)
        cleanUP(udp_DirPath)
        break
    except KeyboardInterrupt:
        print("KeyboardInterrupt")
        cleanUP(udp_DirPath)
        break