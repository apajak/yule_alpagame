# echo-client.py

import socket

# HOST = "54.37.153.179"  # The server's hostname or IP address
HOST = "127.0.0.1"
PORT = 2004  # The port used by the server

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((HOST, PORT))
    #s.sendall(b"*NEW_UDP*")
    data = s.recv(1024)

print(f"Received {data!r}")
