//
//  ViewController.swift
//  Yule
//
//  Created by PAJAK Alexandre on 31/05/2022.
//

import UIKit
import Network


class MainMenuController: UIViewController {
    
    //MARK: - Gobal var

    
    //MARK: - outlet
    @IBOutlet weak var createLobbyButton: UIButton!
    @IBOutlet weak var joinLobbyButton: UIButton!
//        didSet{
//            // Passwd Field design
//            let cornerRAdius = 10
//            disconnectButton.layer.cornerRadius = CGFloat(cornerRAdius)
//            disconnectButton.layer.borderColor = UIColor.init(red: 1, green: 0.49, blue: 0, alpha: 1).cgColor
//            disconnectButton.layer.borderWidth = 2
//            disconnectButton.layer.masksToBounds = (cornerRAdius != 0)
//        }

    
    @IBOutlet weak var settingsButton:UIBarButtonItem!
    
    
    
    @IBOutlet weak var createGameview: UIView!{
        didSet{
            // Passwd Field design
            let cornerRAdius = 10
            createGameview.layer.cornerRadius = CGFloat(cornerRAdius)
            createGameview.layer.borderColor = UIColor.white.cgColor
            createGameview.layer.borderWidth = 2
            createGameview.layer.masksToBounds = (cornerRAdius != 0)
        }
    }
    
    
    @IBOutlet weak var joinGameView: UIView!{
        didSet{
            // Passwd Field design
            let cornerRAdius = 10
            joinGameView.layer.cornerRadius = CGFloat(cornerRAdius)
            joinGameView.layer.borderColor = UIColor.white.cgColor
            joinGameView.layer.borderWidth = 2
            joinGameView.layer.masksToBounds = (cornerRAdius != 0)
        }
    }
    //MARK: - Function
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
    if segue.destination is LobbyController {
    let vc = segue.destination as? LobbyController
        vc?.isMaster = true
            }
    }
    
    
    //MARK: - Action
    @IBAction func createLobby(_ sender: UIButton) {
    }
    @IBAction func joinLobby(_ sender: UIButton) {        
    }
    @IBAction func disconnectButtonTouchUp(_ sender: Any) {
        let user = ConnectController.CurrentUser!
        let data = "*DISCONNECT*" + user.uuid
        let password = ConnectController.TCPclient.getResponse(data: data.data(using: .utf8)!)
        if password == "*DISCONNECT*OK" {
            print("\(user.userName) succesfully disconnected.")
        } else if password == "SERVER: User not found" {
            print("SERVER: \(user.userName) not found")
        } else {
            print("no server response")
        }
    }
    
    
    //MARK: - ViewDidoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        ConnectController.TCPclient.start()
//        ConnectController.CurrentUser = userToObbjectConvertion(user: getUsersList()[0])
//        print(ConnectController.CurrentUser?.userName ?? "toto")
    }
}

    //MARK: - Extention (Delegate)

