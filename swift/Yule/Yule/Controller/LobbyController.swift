//
//  LobbyController.swift
//  Yule
//
//  Created by PAJAK Alexandre on 10/07/2022.
//

import UIKit
    
class UserTableViewCell: UITableViewCell{

    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userView: UIView!{
        didSet{
            // Passwd Field design
            let cornerRAdius = 10
            userView.layer.cornerRadius = CGFloat(cornerRAdius)
            userView.layer.borderColor = UIColor.white.cgColor
            userView.layer.borderWidth = 2
            userView.layer.masksToBounds = (cornerRAdius != 0)
        }
    }
}
class LobbyController: UIViewController {
    
    //MARK: - Gobal var
    var code = ""
    var isMaster = false
    let currentUser = ConnectController.CurrentUser
    var selectedGame: String? = ""
    var playerList:[Player] = []
    var selectGame = ""
    static var udp = UDP(name: "", uuid: "", lobbyCode: "", ip: "", port: 0,master_uuid: "",master_username: "",client_number: 0, udp_client: [])
//    static let UDPclient = UDPClient.init(host: "54.37.153.179", port: 20001)
    static let UDPclient = UDPClient.init(host: "127.0.0.1", port: 20001)
//    static var UDPclient = UDPClient.init(host: udp.ip, port: UInt16(udp.port))
    
    //MARK: - outlet
    @IBOutlet weak var StartButton: UIButton!
    @IBOutlet weak var lobbyCodeLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var waitingPlayersLabel: UILabel!
    
    //MARK: - Action
    @IBAction func StartTouch(_ sender: Any) {
        let data = "*GOGAME*"
        LobbyController.UDPclient.send(data: data.data(using: .utf8)!)
    }
    
    
    //MARK: - Functions
    
    func createLobby(){
//        LobbyController.udp.master_uuid = currentUser!.uuid
//        LobbyController.udp.master_username = currentUser!.userName
//        let request = "*NEW_UDP*" + dumpJSONPUDP(udp: LobbyController.udp)
//        let response = ConnectController.TCPclient.getResponse(data: request.data(using: .utf8)!)
//        LobbyController.udp = parseJSONUDP(jsonString: response)!
        lobby_Connect()
    }
    func getLobby(){
        let request = "*GET_LOBBY*" + code
        let response = ConnectController.TCPclient.getLobby(data: request.data(using: .utf8)!)
        print("response:\(response)")
        LobbyController.udp = parseJSONUDP(jsonString: response)!
        lobby_Connect()
    }
    
    func lobby_Connect(){
        print("Lobby connect")
        LobbyController.UDPclient.start()
        usleep(500)
        let newConnectionResponse = LobbyController.UDPclient.newConnectionRequest(dumpUser: dumpJSONUser(user: currentUser!))
        if newConnectionResponse == "No UDP response" {
            print("UDP connection failed")
        } else {
            let udpJson = newConnectionResponse.components(separatedBy: "*")[2]
            LobbyController.udp = parseJSONUDP(jsonString: udpJson)!
            print("UDP JOIN")
        }
        
    }
    
    func labelActualizer() {
        let playerCount = LobbyController.udp.udp_client.count
        if playerCount > 2 {
            waitingPlayersLabel.text = "Vous êtes \(playerCount) joueurs."
        }
    }
    //MARK: - ViewDidoad
    override func viewDidLoad() {
        super.viewDidLoad()
        print("is master: \(isMaster)")
        print("lobby code: \(code)")
        if isMaster{
//            createLobby()
        } else {
            getLobby()
        }
        lobbyCodeLabel.text = LobbyController.udp.lobbyCode
        if currentUser?.uuid == LobbyController.udp.master_uuid {
            StartButton.isHidden = false
        } else {
            StartButton.isHidden = true
        }
        labelActualizer()
        var background = true
        DispatchQueue.global(qos: .background).async {
            while background{
                var response = UDPClient.serverResponse
                if response.contains("*NEW_LOBBY_CONNECTION*"){
                    let tab = response.split(separator: Character("*"))
                    response = String(tab[1])
                    let udp = parseJSONUDP(jsonString: response)
                    LobbyController.udp = udp!
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                        self.labelActualizer()
                    }
                }
                if response.contains("*GOGAME*"){
                    background = false
                    BarbuController.udp = LobbyController.udp
                    DispatchQueue.main.async {
                        let targetVC = self.storyboard?.instantiateViewController(withIdentifier: "barbuController") as! BarbuController
                        self.navigationController?.pushViewController(targetVC, animated: false)
                    }
                }
                usleep((50000))
                }
            }
        }
    }


    //MARK: - Extention (Delegate)
    let cellSpacingHeight: CGFloat = 30
    let cellReuseIdentifier = "userCell"

    extension LobbyController: UITableViewDataSource{

        func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        }
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return LobbyController.udp.udp_client.count
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! UserTableViewCell
            let user = LobbyController.udp.udp_client[indexPath.row]
            cell.userNameLabel.text = user.userName
            
            return cell
        }
    }

