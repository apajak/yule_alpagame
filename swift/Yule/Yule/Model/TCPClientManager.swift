//
//  TCPClientManager.swift
//  Yule
//
//  Created by PAJAK Alexandre on 03/06/2022.
//
//  * Connect, disconnect, send and receive functions for IOS -> server TCP connection *

import Foundation
import Network

class TCPClient {
    var count : Int
    let connection: ClientConnection
    let host: NWEndpoint.Host
    let port: NWEndpoint.Port
    static var serverResponse = "No TCP reponse"
    
    init(host: String, port: UInt16) {
        self.count = 0
        self.host = NWEndpoint.Host(host)
        self.port = NWEndpoint.Port(rawValue: port)!
        let nwConnection = NWConnection(host: self.host, port: self.port, using: .tcp)
        connection = ClientConnection(nwConnection: nwConnection)
    }
    
    func start() {
        print("TCP Client started \(host) \(port)")
        connection.didStopCallback = didStopCallback(error:)
        connection.start()
    }
    
    func stop() {
        connection.stop()
    }
    func getResponse(data:Data)-> String{
        TCPClient.serverResponse = "No TCP reponse"
        var response = TCPClient.serverResponse
        send(data: data)
        while response == "No TCP reponse"{
            response = TCPClient.serverResponse
        }
        return response
    }
    func getLobby(data:Data)-> String{
        print("get lobby request")
        var response = TCPClient.serverResponse
        send(data: data)
        var i = 0
        while response == "No TCP reponse" || i < 100 {
            i += 1
            response = TCPClient.serverResponse
            usleep(500)
        }
        return response
    }
    // BUG!!!!! need two send for get response !!!! BUG //
    func send(data: Data){
        connection.send(data: data)
    }
    
    func didStopCallback(error: Error?) {
        if error == nil {
            exit(EXIT_SUCCESS)
            //print("TCP disconnected")
        } else {
            exit(EXIT_FAILURE)
        }
    }
}

class ClientConnection {
    
    let  nwConnection: NWConnection
    let TCPqueue = DispatchQueue(label: "TCP Client connection Q")
    
    init(nwConnection: NWConnection) {
        self.nwConnection = nwConnection
    }
    
    var didStopCallback: ((Error?) -> Void)? = nil
    
    func start() {
        print("TCP connection will start")
        nwConnection.stateUpdateHandler = stateDidChange(to:)
        print(setupReceive())
        nwConnection.start(queue: TCPqueue)
    }
    
    private func stateDidChange(to state: NWConnection.State) {
        switch state {
        case .waiting(let error):
            connectionDidFail(error: error)
        case .ready:
            print("TCP Client connection ready")
        case .failed(let error):
            connectionDidFail(error: error)
        default:
            break
        }
    }
    private func setupReceive(){
        nwConnection.receive(minimumIncompleteLength: 1, maximumLength: 65536) { (data, _, isComplete, error) in
            if let data = data, !data.isEmpty {
                let message = String(data: data, encoding: .utf8)
                print("TCP connection did receive, data: \(data as NSData) string: \(message ?? "-" )")
                TCPClient.serverResponse = message ?? "ND"
            }
            if isComplete {
                self.setupReceive()
                self.connectionDidEnd()
            } else if let error = error {
                self.connectionDidFail(error: error)
            } else {
               self.setupReceive()
            }
        }
    }

        
    func send(data: Data){
        nwConnection.send(content: data, completion: .contentProcessed( { error in
            if let error = error {
                self.connectionDidFail(error: error)
                return
            }
                print("TCP connection did send, data: \(data as NSData)")
                self.setupReceive()
            
        }))
    }

    
    func stop() {
        print("TCP connection will stop")
        stop(error: nil)
    }
    
    private func connectionDidFail(error: Error) {
        print("TCP connection did fail, error: \(error)")
        self.stop(error: error)
    }
    
    private func connectionDidEnd() {
        print("TCP connection did end")
        self.stop(error: nil)
    }
    
    private func stop(error: Error?) {
        self.nwConnection.stateUpdateHandler = nil
        self.nwConnection.cancel()
//        if let didStopCallback = self.didStopCallback {
//            self.didStopCallback = nil
//            didStopCallback(error)
//        }
    }
}
