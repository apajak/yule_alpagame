#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------------------------
# Created By  : Pajak Alexandre
# Created Date: 2023/02/13
# version ='1.0'
# ---------------------------------------------------------------------------
""" Python Init Script for Yule Database"""
# ---------------------------------------------------------------------------
# Imports
# ---------------------------------------------------------------------------
import pymysql
from dotenv import load_dotenv
import os

load_dotenv(".env")
YuleAdminPassword = os.getenv("YuleAdminPassword")

class DatabaseManager:
    name = ""
    connection = None
    cursor = None


    def __init__(self, name: str):
        print(YuleAdminPassword)
        try:
            self.name = name
            self.connection = pymysql.connect(
                host="localhost", port=3306, user="YuleAdmin", passwd=YuleAdminPassword, database=name
            )
            self.cursor = self.connection.cursor()
            print(f"-[INIT] {self.name}")
        except pymysql.Error as e:
            print(e)

    def connect(self):
        try:
            self.connection = pymysql.connect(
                host="localhost",
                port=3306,
                user="YuleAdmin",
                passwd=YuleAdminPassword,
                database=self.name,
            )
            self.cursor = self.connection.cursor()
            cursor = self.connection.cursor()
            print(f"-[Connect] {self.name}")
            return cursor
        except pymysql.Error as error:
            print("Error while connecting to sqlite: ", error)

    def disconnect(self):
        try:
            self.connection.close()
            print(f"-[Disconnect] {self.name}")
        except pymysql.Error as error:
            print("Error while disconnecting to sqlite", error)

    def clearData(self):
        try:
            print("+--------------------------------------------------------+")
            print("|                       clear DB                         |")
            print("+--------------------------------------------------------+")
            cursor = self.connection.cursor()
            cursor.execute("DELETE FROM Users")
            self.connection.commit()
            print("-[OK] clear data")
        except pymysql.Error as error:
            print("-[KO] clear data fail: ", error)
        finally:
            print("+--------------------------------------------------------+\n")

    def insertTableUsers(self):
        try:
            cursor = self.connection.cursor()
            cursor.execute("""CREATE TABLE Users (
                                                    uuid              TEXT,
                                                    userName          TEXT,
                                                    firstName         TEXT,
                                                    lastName          TEXT,
                                                    password          TEXT,
                                                    email             TEXT,
                                                    birthDate         DATE    DEFAULT (1 / 1 / 2022),
                                                    gender            TEXT,
                                                    sexualOrientation TEXT,
                                                    coupleState       TEXT,
                                                    alcohol           TEXT,
                                                    totalSips         INTEGER,
                                                    isConnect         BOOLEAN,
                                                    currentTCPAddress TEXT,
                                                    currentUDPAddress TEXT
                                                );""")
            self.connection.commit()
            print("-[OK] create table Users")
        except pymysql.Error as error:
            print("-[KO] create table Users fail: ", error)
        finally:
            print("+--------------------------------------------------------+\n")

yuledatabase = DatabaseManager("Yule")
yuledatabase.insertTableUsers()
